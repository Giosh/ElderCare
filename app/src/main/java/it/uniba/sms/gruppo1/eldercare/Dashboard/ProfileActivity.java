package it.uniba.sms.gruppo1.eldercare.Dashboard;

import android.app.ProgressDialog;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.Date;
import java.text.DateFormat;
import java.text.ParseException;

import it.uniba.sms.gruppo1.eldercare.Constants.CollectionNames;
import it.uniba.sms.gruppo1.eldercare.Models.ProfileInformationModel;
import it.uniba.sms.gruppo1.eldercare.R;
import it.uniba.sms.gruppo1.eldercare.RegistrationActivities.LoginActivity;
import it.uniba.sms.gruppo1.eldercare.SensorDetails.DatePickerFragment;

public class ProfileActivity extends AppCompatActivity {


    private EditText elderName, elderAddress, birthDate, elderPhoneOne, elderPhoneTwo;
    private FirebaseFirestore mDatabase;
    private Toolbar toolbar;
    private Button saveBtn;
    private CollectionReference mElderProfileInformation;
    private ProgressDialog mProgressDialog;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        showProgressDialog();

        //Connecting with Firestore Database
        mDatabase   = FirebaseFirestore.getInstance();

        //Setting up the toolbar
        toolbar     = findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setTitle(getResources().getString(R.string.profile));

        //Linking the resources
        elderName       = findViewById(R.id.elder_name);
        elderAddress    = findViewById(R.id.elder_address);
        elderPhoneOne   = findViewById(R.id.phone_1);
        elderPhoneTwo   = findViewById(R.id.phone_2);
        birthDate       = findViewById(R.id.birth_date);
        saveBtn         = findViewById(R.id.save_profile);

        //Setting the listener on the button that will query the new entries in the Database
        saveBtn.setOnClickListener(saveBtnListener);


        //Initializing the EditText in the activity with the most recent data in the database account
        initializeData();


        //Setting DatePicker when clicking to change the birth date
        birthDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               showDatePickerDialog(v);
            }
        });

    }

    /**
     * this method is called in DatePickerFragment to set the data
     * @param data is the data chosen with the DatePicker
     */
    public void setDate (String data){
        birthDate.setText(data);
    }


    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(true);
        }
        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    /**
     * This method is called in the onCreate and when clicking the save button
     * to initialize the EditText with the most recent data present on the database
     */
    private void initializeData (){

        //Linking the variable with the path on the database where are present the profile information
        mElderProfileInformation = mDatabase.collection(CollectionNames.ELDERS_COLLECTION_NAME).document(LoginActivity.CURRENT_USER).
                collection(CollectionNames.PROFILE_INFORMATIONS);

        mElderProfileInformation.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(QuerySnapshot queryDocumentSnapshots,FirebaseFirestoreException e) {
                mElderProfileInformation.orderBy("lastUpdate").limit(1)
                        .get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        hideProgressDialog();
                        if (!queryDocumentSnapshots.isEmpty()) {
                            ProfileInformationModel profile = new ProfileInformationModel(queryDocumentSnapshots.toObjects(ProfileInformationModel.class));
                            elderAddress.setText((CharSequence) profile.address);
                            elderName.setText((CharSequence) profile.elderName);
                            elderPhoneOne.setText((CharSequence) profile.phoneOne);
                            elderPhoneTwo.setText((CharSequence) profile.phoneTwo);
                            if (profile.birthDate!=null){
                            DateFormat formatter = new java.text.SimpleDateFormat("dd/MM/yyyy");
                            String formattedDate = formatter.format(profile.birthDate);
                            birthDate.setText(formattedDate);
                            }
                        }
                    }
                })
                         .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            hideProgressDialog();
                        }
                    });


            }
        });
    }


    /**
     * This method is used to save the inserted information in the database
     */
    public View.OnClickListener saveBtnListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //Linking the variable with the path on the database where are present the profile information
            mElderProfileInformation = mDatabase.collection(CollectionNames.ELDERS_COLLECTION_NAME).document(LoginActivity.CURRENT_USER).
                    collection(CollectionNames.PROFILE_INFORMATIONS);
            DateFormat formatter = new java.text.SimpleDateFormat("dd/MM/yyyy");
            Date newDate = new Date();
            ProfileInformationModel updated;
            try {
                newDate = (Date) formatter.parse(birthDate.getText().toString());
                updated = new ProfileInformationModel(
                        elderName.getText().toString(),
                        elderAddress.getText().toString(),
                        newDate,
                        elderPhoneOne.getText().toString(),
                        elderPhoneTwo.getText().toString()
                );



            } catch (ParseException e) {
                e.printStackTrace();
                updated = new ProfileInformationModel(
                        elderName.getText().toString(),
                        elderAddress.getText().toString(),
                        null,
                        elderPhoneOne.getText().toString(),
                        elderPhoneTwo.getText().toString()
                );

            }

            mElderProfileInformation.add(updated);
            initializeData();
            String success = getResources().getString(R.string.profile_success_update);
            Toast.makeText(getApplicationContext(),success , Toast.LENGTH_SHORT).show();

        }
    };



    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }


    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
