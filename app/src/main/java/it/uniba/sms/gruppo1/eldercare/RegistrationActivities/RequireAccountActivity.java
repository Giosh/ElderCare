package it.uniba.sms.gruppo1.eldercare.RegistrationActivities;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import it.uniba.sms.gruppo1.eldercare.R;


public class RequireAccountActivity extends AppCompatActivity {

    Button sendMailBtn;
    ImageButton callBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_require_account);

        //Linking buttons with XML resources
        sendMailBtn = findViewById(R.id.btnOK);
        callBtn     =  findViewById(R.id.callBtn);

        // Setting on click listener on buttons
        sendMailBtn.setOnClickListener(sendMailBtnListener);
        callBtn.setOnClickListener(callBtnListener);
    }

    /**
     * This method launch an intent to the mail app with precompiled e-mail and subject
     * in order to contact the company to ask for an account
     */
    public View.OnClickListener sendMailBtnListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //Linking resource files with variables
            String to         = getResources().getString(R.string.contact_name);
            //This passage is necessary because Android requires for the field EXTRA_MAIL a String[]
            String[] strArray = new String[] {to};
            String sub        = getResources().getString(R.string.contact_subject);
            String mess       = ((EditText) findViewById(R.id.txtMessage)).getText().toString();

            Intent mail = new Intent(Intent.ACTION_SEND);

            //Putting information inside the mail intent
            mail.putExtra(Intent.EXTRA_EMAIL, strArray);
            mail.putExtra(Intent.EXTRA_SUBJECT, sub);
            mail.putExtra(Intent.EXTRA_TEXT, mess);
            mail.setType("message/rfc822");

            String sendVia = getResources().getString(R.string.send_mail_via);

            //Starting the mail Intent
            startActivity(Intent.createChooser(mail, sendVia));
        }
    };

    /**
     * This method launch an intent to the dialer with the displayed number on it
     * We chosen this way instead of making a phone call directly so we don't have problems in case
     * the user chooses to not give the Phone permission
     */
    public View.OnClickListener callBtnListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent dialIntent = new Intent(Intent.ACTION_DIAL);
            dialIntent.setData(Uri.parse("tel:" + "+1 234 567 2345"));
            startActivity(dialIntent);
            }
    };
}