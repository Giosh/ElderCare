package it.uniba.sms.gruppo1.eldercare.Dashboard;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import it.uniba.sms.gruppo1.eldercare.Constants.SensorsDetectionsConstants;
import it.uniba.sms.gruppo1.eldercare.Models.SensorDetectionModel;
import it.uniba.sms.gruppo1.eldercare.R;
import it.uniba.sms.gruppo1.eldercare.SensorDetails.SensorDetailsActivity;

import static it.uniba.sms.gruppo1.eldercare.Constants.SensorsDetectionsConstants.SENSOR_TYPE;
import static it.uniba.sms.gruppo1.eldercare.Constants.SensorsDetectionsConstants.SensorTypes.HEARTBEAT_SENSOR;

public class DashboardElderFragment extends DashboardBaseFragment {

    CardView heartCardView;
    CardView caloriesCardView;
    CardView stepsCardView;
    CardView sleepCardView;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        /*This is the static view inflated for the Fragment's layout, with the preset
        * sensors Information. */
        View layout=inflater.inflate(R.layout.grid_layout_elder, container, false);

        heartCardView = layout.findViewById(R.id.heartCardView);
        heartCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SensorDetailsActivity.class);
                intent.putExtra(SENSOR_TYPE, HEARTBEAT_SENSOR);
                startActivity(intent);
            }
        });

        caloriesCardView = layout.findViewById(R.id.caloriesCardView);
        caloriesCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SensorDetailsActivity.class);
                intent.putExtra(SENSOR_TYPE, SensorsDetectionsConstants.SensorTypes.CALORIES_SENSOR);
                startActivity(intent);
            }
        });

        stepsCardView = layout.findViewById(R.id.stepsCardView);
        stepsCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SensorDetailsActivity.class);
                intent.putExtra(SENSOR_TYPE, SensorsDetectionsConstants.SensorTypes.STEPS_COUNTER);
                startActivity(intent);
            }
        });

        sleepCardView = layout.findViewById(R.id.sleepCardView);
        sleepCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SensorDetailsActivity.class);
                intent.putExtra(SENSOR_TYPE, SensorsDetectionsConstants.SensorTypes.SLEEP_SENSOR);
                startActivity(intent);
            }
        });
        return layout;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {}
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void updateUiWithData(ArrayList<SensorDetectionModel> lastSensorsDetections) {
        ElderDashboardElementsUpdater thisActivity = (ElderDashboardElementsUpdater)getActivity();

        if(thisActivity != null)
            thisActivity.updateElderDashboardUI(lastSensorsDetections);
    }
/*This is an interface that the Main Dashboard Activity needs to implement in other to be notified
* whenever the fragment receives new data to be displayed. */
    public interface ElderDashboardElementsUpdater {
        void updateElderDashboardUI(ArrayList<SensorDetectionModel> lastSensorsDetections);
    }
}
