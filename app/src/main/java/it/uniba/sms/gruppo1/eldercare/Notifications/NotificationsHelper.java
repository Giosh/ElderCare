package it.uniba.sms.gruppo1.eldercare.Notifications;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import it.uniba.sms.gruppo1.eldercare.Constants.SensorsDetectionsConstants;
import it.uniba.sms.gruppo1.eldercare.R;
import it.uniba.sms.gruppo1.eldercare.SensorDetails.SensorDetailsActivity;

import static it.uniba.sms.gruppo1.eldercare.Constants.SensorsDetectionsConstants.SENSOR_TYPE;
import static it.uniba.sms.gruppo1.eldercare.Constants.SensorsDetectionsConstants.SensorTypes.HEARTBEAT_SENSOR;
import static it.uniba.sms.gruppo1.eldercare.Constants.SensorsDetectionsConstants.SensorTypes.SLEEP_SENSOR;

/*
* This class helps to keep all the methods needed for notification building,
*   and general operations related to notifications in one single place,
 * instead of simply adding them in the service class. This helps to keep the code simpler
 * and better organized.
 * */
public class NotificationsHelper {
    public static final String CHANNEL_ID="default";
    private static final String TAG ="Notification Helper" ;

    public NotificationsHelper(){

    }
    public static void createNotificationChannel(Context context){
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = context.getString(R.string.channel_name);
            String description = context.getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);

            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this

            try {
                NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
                if (notificationManager != null) {
                    notificationManager.createNotificationChannel(channel);
                }
            }catch (java.lang.NullPointerException e){
                Log.d(TAG,e.getMessage());
            }
        }
    }

    //This method creates the right notification for the specific limit type, that has been reached.

    public static NotificationCompat.Builder notificationBuilder(Context context, String limitType){
        String notificationTitle;
        String notificationContent;
        PendingIntent notificationPendingIntent;

        switch (limitType){
//********************************************************************************************* STEPS_COUNTER-- REQUEST CODE 0
            case SensorsDetectionsConstants.SensorsDetectionLimits.MIN_LIMIT +
                    SensorsDetectionsConstants.SensorTypes.STEPS_COUNTER:
                notificationTitle=context.getString(R.string.your_elder);
                notificationContent=context.getString(R.string.title_steps)+" "+context.getString(R.string.goal_achieved);
                notificationPendingIntent=PendingIntent.getActivity(
                        context, 0, getTheIntentToSensorDetailActivity(
                                context,SensorsDetectionsConstants.SensorTypes.STEPS_COUNTER), 0);
                break;
//********************************************************************************************* CALORIES_SENSOR-- REQUEST CODE 1
            case SensorsDetectionsConstants.SensorsDetectionLimits.MIN_LIMIT +
                    SensorsDetectionsConstants.SensorTypes.CALORIES_SENSOR:
                notificationTitle=context.getString(R.string.your_elder);
                notificationContent=context.getString(R.string.title_calories)+" "+context.getString(R.string.goal_achieved);
                notificationPendingIntent=PendingIntent.getActivity(
                        context, 1, getTheIntentToSensorDetailActivity(
                                context,SensorsDetectionsConstants.SensorTypes.CALORIES_SENSOR), 0);
                break;
//********************************************************************************************* SLEEP_SENSOR-- REQUEST CODE 2
            case SensorsDetectionsConstants.SensorsDetectionLimits.MIN_LIMIT +
                    SLEEP_SENSOR:
                notificationTitle=context.getString(R.string.your_elder);
                notificationContent=context.getString(R.string.title_sleep)+" "+context.getString(R.string.goal_achieved);
                notificationPendingIntent=PendingIntent.getActivity(
                        context, 2, getTheIntentToSensorDetailActivity(
                                context,SensorsDetectionsConstants.SensorTypes.SLEEP_SENSOR), 0);
                break;

//********************************************************************************************* HEARTBEAT_SENSOR-- REQUEST CODE 3
            case SensorsDetectionsConstants.SensorsDetectionLimits.MIN_LIMIT +
                    HEARTBEAT_SENSOR:
                notificationTitle=context.getString(R.string.your_elder);
                notificationContent=context.getString(R.string.title_heartbeat)+" "+context.getString(R.string.passed_min_limit);
                notificationPendingIntent=PendingIntent.getActivity(
                        context, 3, getTheIntentToSensorDetailActivity(
                                context,SensorsDetectionsConstants.SensorTypes.HEARTBEAT_SENSOR), 0);
                break;
            case SensorsDetectionsConstants.SensorsDetectionLimits.MAX_LIMIT +
                    HEARTBEAT_SENSOR:
                notificationTitle=context.getString(R.string.your_elder);
                notificationContent=context.getString(R.string.title_heartbeat)+" "+context.getString(R.string.passed_max_limit);
                notificationPendingIntent=PendingIntent.getActivity(
                        context, 3, getTheIntentToSensorDetailActivity(
                                context,SensorsDetectionsConstants.SensorTypes.HEARTBEAT_SENSOR), 0);
                break;

//********************************************************************************************* TEMPERATURE_SENSOR-- REQUEST CODE 4
            case SensorsDetectionsConstants.SensorsDetectionLimits.MIN_LIMIT +
                    SensorsDetectionsConstants.SensorTypes.TEMPERATURE_SENSOR:
                notificationTitle=context.getString(R.string.your_elder_house);
                notificationContent=context.getString(R.string.title_temperature)+" "+context.getString(R.string.passed_min_limit);
                notificationPendingIntent=PendingIntent.getActivity(
                        context, 4, getTheIntentToSensorDetailActivity(
                                context,SensorsDetectionsConstants.SensorTypes.TEMPERATURE_SENSOR), 0);
                break;
            case SensorsDetectionsConstants.SensorsDetectionLimits.MAX_LIMIT +
                    SensorsDetectionsConstants.SensorTypes.TEMPERATURE_SENSOR:
                notificationTitle=context.getString(R.string.your_elder_house);
                notificationContent=context.getString(R.string.title_temperature)+" "+context.getString(R.string.passed_max_limit);
                notificationPendingIntent=PendingIntent.getActivity(
                        context, 4, getTheIntentToSensorDetailActivity(
                                context,SensorsDetectionsConstants.SensorTypes.TEMPERATURE_SENSOR), 0);
                break;

//********************************************************************************************* MOISTURE_SENSOR-- REQUEST CODE 5
            case SensorsDetectionsConstants.SensorsDetectionLimits.MIN_LIMIT +
                    SensorsDetectionsConstants.SensorTypes.MOISTURE_SENSOR:
                notificationTitle=context.getString(R.string.your_elder_house);
                notificationContent=context.getString(R.string.title_moisture)+" "+context.getString(R.string.passed_min_limit);
                notificationPendingIntent=PendingIntent.getActivity(
                        context, 5, getTheIntentToSensorDetailActivity(
                                context,SensorsDetectionsConstants.SensorTypes.MOISTURE_SENSOR), 0);
                break;
            case SensorsDetectionsConstants.SensorsDetectionLimits.MAX_LIMIT +
                    SensorsDetectionsConstants.SensorTypes.MOISTURE_SENSOR:
                notificationTitle=context.getString(R.string.your_elder_house);
                notificationContent=context.getString(R.string.title_moisture)+" "+context.getString(R.string.passed_max_limit);
                notificationPendingIntent=PendingIntent.getActivity(
                        context, 5, getTheIntentToSensorDetailActivity(
                                context,SensorsDetectionsConstants.SensorTypes.MOISTURE_SENSOR), 0);
                break;

//********************************************************************************************* GAS_SENSOR-- REQUEST CODE 6
            case SensorsDetectionsConstants.SensorsDetectionLimits.MAX_LIMIT +
                    SensorsDetectionsConstants.SensorTypes.GAS_SENSOR:
                notificationTitle=context.getString(R.string.your_elder_house);
                notificationContent=context.getString(R.string.gas_detected);
                notificationPendingIntent=PendingIntent.getActivity(
                        context, 6, getTheIntentToSensorDetailActivity(
                                context,SensorsDetectionsConstants.SensorTypes.GAS_SENSOR), 0);
                break;

//********************************************************************************************* SMOKE_SENSOR-- REQUEST CODE 7
            case SensorsDetectionsConstants.SensorsDetectionLimits.MAX_LIMIT +
                    SensorsDetectionsConstants.SensorTypes.SMOKE_SENSOR:
                notificationTitle=context.getString(R.string.your_elder_house);
                notificationContent=context.getString(R.string.smoke_detected);
                notificationPendingIntent=PendingIntent.getActivity(
                        context, 7, getTheIntentToSensorDetailActivity(
                                context,SensorsDetectionsConstants.SensorTypes.SMOKE_SENSOR), 0);
                break;

            default:
                notificationTitle=null;
                notificationContent=null;
                notificationPendingIntent=null;
                break;
        }

        
        return new NotificationCompat.Builder(context, NotificationsHelper.CHANNEL_ID)
                .setSmallIcon(R.drawable.notification_icon)
                .setContentTitle(notificationTitle)
                .setContentText(notificationContent)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(notificationPendingIntent)
                .setAutoCancel(true);
    }


// ge the intent to start the right sensorDetailActivity after the notification tap.
    private static Intent getTheIntentToSensorDetailActivity(Context context, String sensorType){
        Intent intent = new Intent(context, SensorDetailsActivity.class);
        intent.putExtra(SENSOR_TYPE, sensorType);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        return intent;


    }

}
