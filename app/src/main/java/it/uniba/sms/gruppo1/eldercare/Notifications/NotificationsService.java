package it.uniba.sms.gruppo1.eldercare.Notifications;

import android.annotation.SuppressLint;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import android.os.Process;


import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.List;
import java.util.Objects;

import it.uniba.sms.gruppo1.eldercare.Constants.CollectionNames;
import it.uniba.sms.gruppo1.eldercare.Constants.SensorsDetectionsConstants;
import it.uniba.sms.gruppo1.eldercare.Constants.SharedPreferencesNames;
import it.uniba.sms.gruppo1.eldercare.Models.SensorDetectionModel;

import static it.uniba.sms.gruppo1.eldercare.Notifications.LimitsChecker.NO_NOTIFICATION;

public class NotificationsService extends Service {


    public static final String TAG="Service";
    public static final String TAG_DI_QUESTO_TEST=" SERVICEE ";

    //autoincrement unique ID for notifications.
    public static int notificationId=0;

    private ServiceHandler mServiceHandler;
    //Reference to the sensors detections collection of this current Elder
    private CollectionReference mElderSensorsDetections;
    //Listener to the collection.
    private ListenerRegistration registration;
    //Necessary variable to send notifications
    private NotificationManagerCompat notificationManagerCompat;
    //This variable is behind the logic of how this service works. Whenever the service starts,
    // the addSnapShotListener method retrieves all the documents
    //in the sensorsDetections collection, so if we don't keep track of which detections have already
    //been checked, it is possible that the service will produce notifications for stale detections
    //for which the user has, probably, already received the notification. This variable contains
    //the exact moment at which the service has read the last sensorDetection.
    private long lastDetectionReadMoment;


    public NotificationsService() {
    }

    @Override
    public void onCreate() {
        /*This method is called only once, util the service is not killed by the system, in which case
        * it will be recreated as soon as possible. So we need to initialize all the variables
        * that will be used every time the startCommand and so the HandleMessage methods are called.*/

    /*--------------First thing first set up the lastDetectionReadMoment Variable---------------- */
    //If the variable does'nt already exists in the shared preferences, than we set it up
    //with the current calendar time.
        if(getSharedPreferences(SharedPreferencesNames.LAST_DETECTION_READ_MOMENT_PREFERENCES,MODE_PRIVATE).getLong(SharedPreferencesNames.LAST_DETECTION_READ_MOMENT,0)!=0){
            lastDetectionReadMoment=getSharedPreferences(SharedPreferencesNames.LAST_DETECTION_READ_MOMENT_PREFERENCES,MODE_PRIVATE).getLong(SharedPreferencesNames.LAST_DETECTION_READ_MOMENT,0);
        }else
            lastDetectionReadMoment=System.currentTimeMillis();

    /*--------------Operations to set up Notifications---------------- */
        NotificationsHelper.createNotificationChannel(this);
        notificationManagerCompat = NotificationManagerCompat.from(this);

    /*--------------Operations to set up Firestore variables---------------- */

        FirebaseFirestore mDatabase = FirebaseFirestore.getInstance();

        String currentUserEmail=null;
        try {
            currentUserEmail = Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getEmail();
        }catch (NullPointerException e){
            this.stopSelf();
        }

        if (currentUserEmail!=null){
            mElderSensorsDetections = mDatabase.collection(CollectionNames.ELDERS_COLLECTION_NAME).
                    document(currentUserEmail).
                    collection(CollectionNames.SENSORS_DETECTIONS);
        }else this.stopSelf();

        // Start up the thread running the service. Note that we create a
        // separate thread because the service normally runs in the process's
        // main thread, which we don't want to block. We also make it
        // background priority so CPU-intensive work doesn't disrupt our UI.

        HandlerThread thread = new HandlerThread("ServiceStartArguments",
                Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();

        // Get the HandlerThread's Looper and use it for our Handler
        Looper mServiceLooper = thread.getLooper();
        mServiceHandler = new ServiceHandler(mServiceLooper, this);


    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // For each start request, send a message    to start a job and deliver the
        // start ID so we know which request we're stopping when we finish the job
        Message msg = mServiceHandler.obtainMessage();
        msg.arg1 = startId;
        mServiceHandler.sendMessage(msg);

        // If we get killed, after returning from here, restart
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("SSSS Not yet implemented");
    }


    @SuppressLint("ApplySharedPref")
    @Override
    public void onDestroy() {
     super.onDestroy();
    //frees the firestore snapshot listener
        registration.remove();
    //This method is called right before the service gets killed by the system, so it is the right
    //moment to save the lastDetectionReadMoment, so that is can be restore in the next creation.
        SharedPreferences lastDetectionReadMomentPreferences = getSharedPreferences(SharedPreferencesNames.LAST_DETECTION_READ_MOMENT_PREFERENCES, MODE_PRIVATE);
        SharedPreferences.Editor editor1= lastDetectionReadMomentPreferences.edit();
        editor1.putLong(SharedPreferencesNames.LAST_DETECTION_READ_MOMENT, lastDetectionReadMoment);
        editor1.commit();
    }

    private final class ServiceHandler extends Handler {
        Context context;
        ServiceHandler(Looper looper, Context context) {
            super(looper);
            this.context=context;
        }

        @Override
        public void handleMessage(Message msg) {
            //add the snapshot listener to get all the new incoming sensors detections, so that we
            //we can check them against the thresholds.
            registration=mElderSensorsDetections.addSnapshotListener(new EventListener<QuerySnapshot>() {
                @Override
                public void onEvent(QuerySnapshot queryDocumentSnapshots, FirebaseFirestoreException e) {
                    //Calendar calWithinOnEvent=Calendar.getInstance();
                    String notificationType;
                    LimitsChecker limitsChecker=new LimitsChecker();
                    SleepDetectionsHelper sleepDetectionsHelper=new SleepDetectionsHelper();
                    List<DocumentChange> lastAddedSensorsDetections = queryDocumentSnapshots.getDocumentChanges();
                    for (DocumentChange d : lastAddedSensorsDetections) {
                        try {

                            SensorDetectionModel sensorDetection = d.getDocument().toObject(SensorDetectionModel.class);
                            //We treat sleep detections slightly differently. Since we check the overall sum of
                            //sleep hours, whether their heavy sleep detections or soft sleep detection, for every given day, we
                            //don't need to check whether the sensor detection has already been read or not,
                            //we simply recalculate the sum for the current day, and check if the goal has been
                            //achieved or not.
                            if(SleepDetectionsHelper.isSleepDetection(sensorDetection.sensorType)) {
                                notificationType=SleepDetectionsHelper.checkSleepLimit(
                                        getSharedPreferences(SharedPreferencesNames.LIMIT_PREFERENCES,MODE_PRIVATE),
                                        sleepDetectionsHelper.getCurrentOverallSleepHours(mElderSensorsDetections));
                                if (!(notificationType.equals(NO_NOTIFICATION)))
                                    notificationManagerCompat.notify(notificationId++, NotificationsHelper.notificationBuilder(context,notificationType).build());

                            }else {
                                //For all the other detection types we check whether we already read the given detection, if so we just skip it.
                                //Otherwise we check their value against the specific thresholds, and eventually we send a notification.
                                if(sensorDetection.timestamp.getTime()> lastDetectionReadMoment){

                                    if(isElderSensorDetection(sensorDetection.sensorType)){
                                        notificationType=limitsChecker.checkThisElderDetection(sensorDetection,getSharedPreferences(SharedPreferencesNames.LIMIT_PREFERENCES,MODE_PRIVATE));
                                        if (!(notificationType.equals(NO_NOTIFICATION)))
                                            notificationManagerCompat.notify(notificationId++, NotificationsHelper.notificationBuilder(context,notificationType).build());

                                    }else {
                                        notificationType=limitsChecker.checkThisHomeDetection(sensorDetection,getSharedPreferences(SharedPreferencesNames.LIMIT_PREFERENCES,MODE_PRIVATE));
                                        if (!(notificationType.equals(NO_NOTIFICATION)))
                                            notificationManagerCompat.notify(++notificationId, NotificationsHelper.notificationBuilder(context,notificationType).build());
                                    }
                                }
                            }

                        } catch (RuntimeException exc) {
                            Log.e(TAG, TAG_DI_QUESTO_TEST + exc.getMessage());
                        }
                    }
                    //save the moment of this check.
                    lastDetectionReadMoment =System.currentTimeMillis();
                }

            });
        }
    }


    public boolean isElderSensorDetection(String sensorDetectionType){
        return sensorDetectionType.equals(SensorsDetectionsConstants.SensorTypes.CALORIES_SENSOR) ||
                sensorDetectionType.equals(SensorsDetectionsConstants.SensorTypes.STEPS_COUNTER) ||
                sensorDetectionType.equals(SensorsDetectionsConstants.SensorTypes.HEARTBEAT_SENSOR);
    }



}

