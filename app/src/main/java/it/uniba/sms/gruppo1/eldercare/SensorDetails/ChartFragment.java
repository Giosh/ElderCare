package it.uniba.sms.gruppo1.eldercare.SensorDetails;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import it.uniba.sms.gruppo1.eldercare.Constants.CollectionNames;
import it.uniba.sms.gruppo1.eldercare.Constants.SensorsDetectionsConstants;
import it.uniba.sms.gruppo1.eldercare.RegistrationActivities.LoginActivity;
import it.uniba.sms.gruppo1.eldercare.Models.SensorDetectionModel;
import it.uniba.sms.gruppo1.eldercare.R;

import static it.uniba.sms.gruppo1.eldercare.R.color.secondaryLightColor;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ChartFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ChartFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 *
 * Description: this class is used to generate the chart requesting data from Firestore database.
 */
public class ChartFragment extends Fragment {

    //constants used when putting and getting extras from Bundle
    private static final String ARG_SENSOR_TYPE = "sensorType";
    private static final String ARG_DATE        = "date";

    //Fragment TAG used to print infos with Log.d
    private static final String TAG             = "Chart fragment";

    //Reference to collection: elders/{user}/sensorsDetections
    private CollectionReference mElderSensorsDetections;

    /**
     * Description: Class attribute used to set the sensorType. When the user clicks on a card in the
     * DashboardActivity this parameter is passed to SensorsDetailsActivity and then is passed in this class
     * through the constructor newInstance()
     * */
    private String sensorType;
    private String date;
    private Calendar cal;

    // Inizializing with today
    private Timestamp endOfTheDay   = new Timestamp(new Date());
    private Timestamp startOfTheDay = new Timestamp(new Date());
    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     */
    LineChart chart;
    public ChartFragment() {}
    public static ChartFragment newInstance(String sensorType, String date) {
        ChartFragment fragment = new ChartFragment();
        Bundle args            = new Bundle();
        args.putString(ARG_SENSOR_TYPE, sensorType);
        args.putString(ARG_DATE, date);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Description: method that requires a Date object and returns a formatted hh:mm String
     * @param date Date object
     * @return String that represents hours and minutes in hh:mm format
     */
    public String getFormattedHours(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int hours         = calendar.get(Calendar.HOUR_OF_DAY);
        int minutes       = calendar.get(Calendar.MINUTE);
        return String.format(Locale.ITALIAN, "%02d:%02d", hours, minutes);
    }

    /**
     * Description: this method set data on the chart.It takes an ArrayList as parameter,
     * formats data and labels and then setup the chart
     * @param list list of data of a given sensor
     */
    public void setData(ArrayList<SensorDetectionModel> list) {
        List<Entry> entries = new ArrayList<>();
        final String[] dataLabels = new String[list.size()];
        int i=0;

        for(SensorDetectionModel s: list) {
            entries.add(new Entry((float)(i), (float)(s.value)));
            dataLabels[i] = getFormattedHours(list.get(i).timestamp);
            i++;
        }

        IAxisValueFormatter formatter = new IAxisValueFormatter() {

            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return dataLabels[(int) value];
            }
        };

        // Getting the X axis in order to properly put labels in the bottom, formatting values
        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setValueFormatter(formatter);

        // Getting the Y axis in order to properly put labels in the left, formatting values
        YAxis yAxisright = chart.getAxisRight();
        yAxisright.setDrawLabels(false);

        // Putting values in the dataset of the graph, customizing colors
        LineDataSet dataSet = new LineDataSet(entries, "");
        dataSet.setDrawFilled(true);
        dataSet.setDrawValues(false);

        //sometimes getResource() throws an error if no context has been passed
        if(isAdded()) {
            dataSet.setFillColor(getResources().getColor(secondaryLightColor));
            dataSet.setColor(getResources().getColor(R.color.secondaryDarkColor));
            dataSet.setCircleColor(getResources().getColor(R.color.primaryColor));
        }

        dataSet.setAxisDependency(YAxis.AxisDependency.LEFT);

        Legend legend = chart.getLegend();
        legend.setEnabled(false);

        // Putting the dataset in the graph, then invalidating in order to refresh it
        LineData lineData = new LineData(dataSet);
        chart.setData(lineData);
        chart.setDrawGridBackground(true);
        chart.setDescription(null);
        chart.invalidate(); // refresh
    }

    /**
     * Description: when creating the view, a database instance is setted up.
     * It checks for arguments (sensorType or date) and if not null it calls up getLatestDetectionValues() methods
     * which make the request to retrieve data.
     * It will also add a snapshotListener: when data change in a certain collection/document it will execute the method in the
     * anonymous class
     */
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        FirebaseFirestore mDatabase = FirebaseFirestore.getInstance();
        mElderSensorsDetections = mDatabase.collection(CollectionNames.ELDERS_COLLECTION_NAME)
                                           .document(SensorDetailsActivity.CURRENT_USER)
                                           .collection(CollectionNames.SENSORS_DETECTIONS);

        if (getArguments() != null) {
            this.cal        = Calendar.getInstance();
            this.sensorType = getArguments().getString(ARG_SENSOR_TYPE);
            this.date       = getArguments().getString(ARG_DATE);

            // Getting detections from Firebase with the pointer to the relative collection
            //getLatestDetectionValues(mElderSensorsDetections);

            // Inizialize the calendar, in order to set the properties for the filter
            try {
                cal.setTime(formatter.parse(date));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            // The startOfTheDay is inizialized with the selected day at 00:00:00
            this.startOfTheDay = new Timestamp(cal.getTime());

            // The endOfTheDay is inizialized with the selected day but at 23.59.59
            cal.set(Calendar.HOUR_OF_DAY, 23);
            cal.set(Calendar.MINUTE,      59);
            cal.set(Calendar.SECOND,      59);
            this.endOfTheDay = new Timestamp(cal.getTime());
        }

        // This is the query, filtering by sensorType and by the entire day
        mElderSensorsDetections.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(QuerySnapshot queryDocumentSnapshots,FirebaseFirestoreException e) {
                mElderSensorsDetections.whereEqualTo("sensorType", sensorType)
                        .whereGreaterThanOrEqualTo("timestamp", startOfTheDay)
                        .whereLessThanOrEqualTo("timestamp", endOfTheDay)
                        .orderBy(SensorsDetectionsConstants.TIMESTAMP)
                        .get()
                        .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                            @Override
                            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                                ArrayList<SensorDetectionModel> dataToDisplay = new ArrayList<>(queryDocumentSnapshots.toObjects(SensorDetectionModel.class));
                                if(!dataToDisplay.isEmpty())
                                    setData(dataToDisplay);

                                if(getActivity() != null) {
                                    ((SensorDetailsActivity)getActivity()).hideProgressDialog();
                                }
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.d(TAG, e.getMessage());
                            }
                        });
            }
        });

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_chart, container, false);
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    /**
     * Description: method called first time the fragment is setted up. It's used to retrieve data from firestore database
     * The query takes data from elders/{user}/sensorDetections and it filters documents to find the matching sensor type.
     * @param thisElderSensorsDetections CollectionReference object that references to elders/{user}/sensorDetections
     */

    // @TODO: Eventually needs to add filtering by date!!!
    public void getLatestDetectionValues(CollectionReference thisElderSensorsDetections) {
        thisElderSensorsDetections
                .whereEqualTo("sensorType", this.sensorType)
                .whereGreaterThanOrEqualTo("timestamp", startOfTheDay)
                .whereLessThanOrEqualTo("timestamp", endOfTheDay)
                .orderBy(SensorsDetectionsConstants.TIMESTAMP)
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        ArrayList<SensorDetectionModel> dataToDisplay = new ArrayList<>(queryDocumentSnapshots.toObjects(SensorDetectionModel.class));
                        if(!dataToDisplay.isEmpty())
                            setData(dataToDisplay);

                        if(getActivity() != null) {
                            ((SensorDetailsActivity)getActivity()).hideProgressDialog();
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d(TAG, e.getMessage());
                    }
                });
    }

    /**
     * Description: this method takes the chart reference from XML file.
     */
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        chart = view.findViewById(R.id.chart);
    }

    @Override
    public void onStart() {
        super.onStart();
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

}
