package it.uniba.sms.gruppo1.eldercare.Notifications;

import android.content.SharedPreferences;
import android.util.Log;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Calendar;

import it.uniba.sms.gruppo1.eldercare.Constants.SensorsDetectionsConstants;
import it.uniba.sms.gruppo1.eldercare.Models.SensorDetectionModel;

import static it.uniba.sms.gruppo1.eldercare.Constants.SensorsDetectionsConstants.SENSOR_TYPE;
import static it.uniba.sms.gruppo1.eldercare.Constants.SensorsDetectionsConstants.SensorTypes.SLEEP_SENSOR;
import static it.uniba.sms.gruppo1.eldercare.Constants.SensorsDetectionsConstants.TIMESTAMP;
import static it.uniba.sms.gruppo1.eldercare.Notifications.LimitsChecker.NO_NOTIFICATION;
import static it.uniba.sms.gruppo1.eldercare.Notifications.LimitsChecker.TAG;

/*This class contains all the methods need to perform the checkup on sleep data*/
public class SleepDetectionsHelper {

    //the current sum of sleep hours, where overAll stands for both heavy sleep and soft sleep hours.
    //The sum is calculated on a daily basis, which means that it starts at the beginning of the day
    //at 00:00:00 and ends up at 23:59:59.
    private long overAllHoursSum;

    //get the sum of the sleep hours in this moment of this specific day.
    public int getCurrentOverallSleepHours(CollectionReference mElderSensorsDetections){
        Calendar cal=Calendar.getInstance();
        Timestamp endOfTheDay;
        Timestamp startOfTheDay;

        // The startOfTheDay is initialized with the current day at 00:00:00
        startOfTheDay = new Timestamp(cal.getTime());

        // The endOfTheDay is initialized with the current day but at 23.59.59
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE,      59);
        cal.set(Calendar.SECOND,      59);
        endOfTheDay = new Timestamp(cal.getTime());

        //We perform a query on Firestore senorsDetections collection for this current elder.
        //This query retrieves all the sleep sensor detections, for this current day. We than
        //calculate the sum of sleep hours passed so far, by adding up all the intervals that
        //passe from a given detection and the previous one, until we arrive at the end of the
        //detections list.
        mElderSensorsDetections.whereEqualTo(SENSOR_TYPE, SLEEP_SENSOR)
                .whereGreaterThanOrEqualTo(TIMESTAMP, startOfTheDay)
                .whereLessThanOrEqualTo(TIMESTAMP, endOfTheDay)
                .orderBy(SensorsDetectionsConstants.TIMESTAMP)
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        ArrayList<SensorDetectionModel> sleepDetections =
                                new ArrayList<>(queryDocumentSnapshots.toObjects(SensorDetectionModel.class));

                        Calendar calWithinOnSuccess=Calendar.getInstance();
                        long overAllHoursSum=0;
                        long thisDetectionTimestamp;
                        long previousDetectionTimestamp;
                        int i=0;

                        for(SensorDetectionModel sleepDetection: sleepDetections){
                            i++;
                            if(sleepDetection.value==SensorsDetectionsConstants.SleepSensorConstants.SOFT_SLEEP ||
                                    sleepDetection.value==SensorsDetectionsConstants.SleepSensorConstants.HEAVY_SLEEP){

                                calWithinOnSuccess.setTime(sleepDetection.timestamp);
                                thisDetectionTimestamp=calWithinOnSuccess.getTimeInMillis();
                                calWithinOnSuccess.setTime(sleepDetections.get(i).timestamp);
                                previousDetectionTimestamp=calWithinOnSuccess.getTimeInMillis();
                                overAllHoursSum+=thisDetectionTimestamp-previousDetectionTimestamp;
                                setOverAllHoursSum(overAllHoursSum);
                            }

                        }
                    }
                });

        return getOverAllHoursSum();

    }

    private void setOverAllHoursSum(long overAllHoursSum1){
        this.overAllHoursSum=overAllHoursSum1;
    }

    private int getOverAllHoursSum(){
        return (int)((this.overAllHoursSum/(1000*60*60)) );
    }


    public static boolean isSleepDetection(String sensorDetectionType){
        return sensorDetectionType.equals(SLEEP_SENSOR);
    }

    //checker method for sleep limits. If the overall sum of hours is greater or equal to the set
    //goal, than we notify the user that the goal has, indeed, been reached, otherwise
    // we send no notification.
    public static String checkSleepLimit(SharedPreferences limitsSharedPreferences, int overAllSleepHours){
        int minSleepLimit=limitsSharedPreferences.getInt
                (SensorsDetectionsConstants.SensorsDetectionLimits.MIN_LIMIT +
                        SensorsDetectionsConstants.SensorTypes.SLEEP_SENSOR, 0);

        if(minSleepLimit!=0){
            if(overAllSleepHours>=minSleepLimit){
                return SensorsDetectionsConstants.SensorsDetectionLimits.MIN_LIMIT +
                        SensorsDetectionsConstants.SensorTypes.SLEEP_SENSOR;
            }else
                return NO_NOTIFICATION;
        }else
            return NO_NOTIFICATION;

    }

}
