package it.uniba.sms.gruppo1.eldercare.SensorDetails;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;
import java.util.Calendar;
import java.util.Locale;

import it.uniba.sms.gruppo1.eldercare.Dashboard.ProfileActivity;
import it.uniba.sms.gruppo1.eldercare.R;

public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year         = c.get(Calendar.YEAR);
        int month        = c.get(Calendar.MONTH);
        int day          = c.get(Calendar.DAY_OF_MONTH);


        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(getActivity(), R.style.DialogTheme, this, year, month, day);
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        // Set the date chosen by the user
        if(getActivity() != null)
            if(getContext().getClass()==SensorDetailsActivity.class) {
                ((SensorDetailsActivity) getActivity()).update(String.format(Locale.ITALIAN, "%d/%d/%d", day, (month + 1), year));
                ((SensorDetailsActivity) getActivity()).showProgressDialog();
            }
        if(getContext().getClass()==ProfileActivity.class) {
            ((ProfileActivity) getActivity()).setDate(String.format(Locale.ITALIAN, "%d/%d/%d", day, (month + 1), year));
        }
    }
}