package it.uniba.sms.gruppo1.eldercare.Dashboard;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import it.uniba.sms.gruppo1.eldercare.Constants.SensorsDetectionsConstants;
import it.uniba.sms.gruppo1.eldercare.Models.SensorDetectionModel;
import it.uniba.sms.gruppo1.eldercare.R;
import it.uniba.sms.gruppo1.eldercare.SensorDetails.SensorDetailsActivity;

import static it.uniba.sms.gruppo1.eldercare.Constants.SensorsDetectionsConstants.SENSOR_TYPE;

public class DashboardHomeFragment extends DashboardBaseFragment {
    CardView temperatureCardView,
             gasCardView,
             moistureCardView,
             smokeCardView;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View layout = inflater.inflate(R.layout.grid_layout_home, container, false);
        /*This is the static view inflated for the Fragment's layout, with the preset
         * sensors Information. */
        temperatureCardView = layout.findViewById(R.id.temperatureCardView);
        temperatureCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SensorDetailsActivity.class);
                intent.putExtra(SENSOR_TYPE, SensorsDetectionsConstants.SensorTypes.TEMPERATURE_SENSOR);
                startActivity(intent);
            }
        });

        gasCardView = layout.findViewById(R.id.gasCardView);
        gasCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SensorDetailsActivity.class);
                intent.putExtra(SENSOR_TYPE, SensorsDetectionsConstants.SensorTypes.GAS_SENSOR);
                startActivity(intent);
            }
        });

        moistureCardView = layout.findViewById(R.id.moistureCardView);
        moistureCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SensorDetailsActivity.class);
                intent.putExtra(SENSOR_TYPE, SensorsDetectionsConstants.SensorTypes.MOISTURE_SENSOR);
                startActivity(intent);
            }
        });

        smokeCardView = layout.findViewById(R.id.smokeCardView);
        smokeCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SensorDetailsActivity.class);
                intent.putExtra(SENSOR_TYPE, SensorsDetectionsConstants.SensorTypes.SMOKE_SENSOR);
                startActivity(intent);
            }
        });

        return layout;
    }

    @Override
    public void updateUiWithData(ArrayList<SensorDetectionModel> lastSensorsDetections) {
        HomeDashboardElementsUpdater thisActivity=(HomeDashboardElementsUpdater)getActivity();

        if(thisActivity != null)
            thisActivity.updateHomeDashboardUI(lastSensorsDetections);
    }

    /*This is an interface that the Main Dashboard Activity needs to implement in other to be notified
     * whenever the fragment receives new data to be displayed. */
    public interface HomeDashboardElementsUpdater {
        void updateHomeDashboardUI(ArrayList<SensorDetectionModel> lastSensorsDetections);
    }
}
