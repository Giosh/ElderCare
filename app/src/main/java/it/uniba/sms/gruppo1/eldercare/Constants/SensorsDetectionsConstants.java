package it.uniba.sms.gruppo1.eldercare.Constants;

/*These constants represents some of the fields names used in the FireStore Collections.*/
public class SensorsDetectionsConstants {
    //The sensorsDetections collection contains many different documents (sensorDetection),
    //each of which contains three fields representing the specific sensor detection.
    //Timestamp and sensorType are two of these. The other one is value
    public static final String TIMESTAMP="timestamp";
    public static final String SENSOR_TYPE="sensorType";

    //The field sensorsType, which is of type String, can only contain one of the following
    //constants as its value.
    public static final class SensorTypes {
        public static final String HEARTBEAT_SENSOR="heart";
        public static final String STEPS_COUNTER="steps";
        public static final String SLEEP_SENSOR="sleep";
        public static final String TEMPERATURE_SENSOR="temperature";
        public static final String MOISTURE_SENSOR="moisture";
        public static final String SMOKE_SENSOR="smoke";
        public static final String GAS_SENSOR="gas";
        public static final String CALORIES_SENSOR="calories";
    }

    /*These inner static class contains constants used in saving and checking limits on the sensors
    * detections values. */
    public static  final class SensorsDetectionLimits{
        public static final String MIN_LIMIT="minLimit";
        public static final String MAX_LIMIT="maxLimit";
        public static final float MAX_SMOKE_PERCENTAGE=20;
        public static final float MAX_GAS_PERCENTAGE=15;
    }

    /*These are the constants used to interpret the values of the sleep sensors detections*/
    public static final class SleepSensorConstants{
        public static final int HEAVY_SLEEP=2;
        public static final int SOFT_SLEEP=1;
        public static final int AWAKE=0;

    }



}
