package it.uniba.sms.gruppo1.eldercare.Constants;

public final class SharedPreferencesNames {

    /*LIMITS PREFERENCES store the threshold related to sensors Detections  set by the caregiver.
     * The possible values stored in this SharedPreferences instance are:
     * ----------------THRESHOLDS RELATED TO ELDER'S VALUES----------------------
      * 1.   KEY: SensorsDetectionsConstants.SensorsDetectionLimits.MAX_LIMIT +
                        SensorsDetectionsConstants.SensorTypes.HEARTBEAT_SENSOR
        2.  KEY: SensorsDetectionsConstants.SensorsDetectionLimits.MIN_LIMIT +
                        SensorsDetectionsConstants.SensorTypes.HEARTBEAT_SENSOR
        3.  KEY: SensorsDetectionsConstants.SensorsDetectionLimits.MIN_LIMIT +
                        SensorsDetectionsConstants.SensorTypes.STEPS_COUNTER
        4.  KEY: SensorsDetectionsConstants.SensorsDetectionLimits.MIN_LIMIT +
                        SensorsDetectionsConstants.SensorTypes.SLEEP_SENSOR
        5.  KEY: SensorsDetectionsConstants.SensorsDetectionLimits.MIN_LIMIT +
                        SensorsDetectionsConstants.SensorTypes.CALORIES_SENSOR

        ALL THESE KEYS ACCEPT
        VALUE TYPE= int
        DEFAULT= 0--> it means that no preference exist for this VALUE

        ----------------THRESHOLDS RELATED TO ELDER'S HOUSE VALUES----------------------
        1.  KEY: SensorsDetectionsConstants.SensorsDetectionLimits.MIN_LIMIT +
                        SensorsDetectionsConstants.SensorTypes.TEMPERATURE_SENSOR
        2.  KEY:SensorsDetectionsConstants.SensorsDetectionLimits.MAX_LIMIT +
                        SensorsDetectionsConstants.SensorTypes.TEMPERATURE_SENSOR
        THESE TWO KEYS ACCEPT
        VALUE TYPE= int
        DEFAULT= 0--> it means that no preference exist for this VALUE

        3.  KEY: SensorsDetectionsConstants.SensorsDetectionLimits.MIN_LIMIT +
                        SensorsDetectionsConstants.SensorTypes.MOISTURE_SENSOR
        4.  KEY:SensorsDetectionsConstants.SensorsDetectionLimits.MAX_LIMIT +
                        SensorsDetectionsConstants.SensorTypes.MOISTURE_SENSOR

        THESE TWO KEYS ACCEPT
        VALUE TYPE= int
        DEFAULT= 0--> it means that no preference exist for this VALUE

        5.  KEY: SensorsDetectionsConstants.SensorsDetectionLimits.MAX_LIMIT +
                SensorsDetectionsConstants.SensorTypes.GAS_SENSOR
        6. KEY: SensorsDetectionsConstants.SensorsDetectionLimits.MAX_LIMIT +
                SensorsDetectionsConstants.SensorTypes.SMOKE_SENSOR
        THESE LAST TWO KEYS ACCEPT
        VALUE TYPE= boolean
        DEFAULT= false--> it means that no check needs to be done on the house gas or
         smoke prefixed percentage limit


        */
    public static final String LIMIT_PREFERENCES="limit_preferences";


      /*LAST_DETECTION_READ_MOMENT_PREFERENCES: tracks the last moment that the Notification Service
        checked for a set threshold on the incoming SensorsDetections.
        This is needed because if the service is shut down by the system, the service in OnDestroy
        saves the last moment he checked the incoming Detections, in order to avoid rechecking
        detections that he already checked (that's because Firestore retrieves all the sensorsDetections
        everyTime the service is started.

            The only Key-Value pair for this SharedPreferences Instance:
            KEY: LAST_DETECTION_READ_MOMENT
            VALUE TYPE: Long
        */

    public static final String LAST_DETECTION_READ_MOMENT_PREFERENCES="lastDetectionReadMoment";
    public static final String LAST_DETECTION_READ_MOMENT="lastDetectionReadMoment";



}
