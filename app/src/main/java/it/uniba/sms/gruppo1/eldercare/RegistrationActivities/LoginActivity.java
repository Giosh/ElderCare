package it.uniba.sms.gruppo1.eldercare.RegistrationActivities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;


import it.uniba.sms.gruppo1.eldercare.Dashboard.DashboardActivity;
import it.uniba.sms.gruppo1.eldercare.R;

public class LoginActivity extends BaseActivity {

    public static String CURRENT_USER;
    private EditText mEmailField;
    private EditText mPasswordField;
    // [START declare_auth]
    private FirebaseAuth mAuth;
    // [END declare_auth]

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FirebaseApp.initializeApp(this);

        setContentView(R.layout.activity_login);

        mEmailField = findViewById(R.id.field_email);
        mPasswordField = findViewById(R.id.field_password);

        // Linking button with XML resoures
        Button signInBtn = findViewById(R.id.email_sign_in_button);
        Button forgotPasswordBtn = findViewById(R.id.forgot_password_button);
        Button requireAccountBtn = findViewById(R.id.require_account_btn);

        //Setting onClick Listener on Buttons
        signInBtn.setOnClickListener(signInBtnListener);
        forgotPasswordBtn.setOnClickListener(forgotPasswordBtnListener);
        requireAccountBtn.setOnClickListener(requireAccountBtnListener);

        // [START initialize_auth]
        mAuth = FirebaseAuth.getInstance();
        // [END initialize_auth]
    }

    // [START on_start_check_user]
    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        if (mAuth.getCurrentUser() != null) {
            CURRENT_USER=mAuth.getCurrentUser().getEmail();
            onAuthSuccess();
        }
    }
    // [END on_start_check_user]
    private void signIn(String email, String password) {
        if (!validateForm()) {
            return;
        }
        showProgressDialog();
        // [START sign_in_with_email]
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            FirebaseUser user = mAuth.getCurrentUser();
                            if(user != null)
                                CURRENT_USER=user.getEmail();
                            Intent goDashBoard= new Intent(LoginActivity.this, DashboardActivity.class);
                            startActivity(goDashBoard);
                            finish();
                        } else {
                            // If sign in fails, display a message to the user.
                            Toast.makeText(LoginActivity.this, R.string.auth_failed,
                                    Toast.LENGTH_SHORT).show();
                        }

                        // [START_EXCLUDE]
                        if (!task.isSuccessful()) {
                            Toast.makeText(LoginActivity.this, R.string.auth_failed,
                                    Toast.LENGTH_SHORT).show();
                        }
                        hideProgressDialog();
                        // [END_EXCLUDE]
                    }
                });
        // [END sign_in_with_email]
    }
    private boolean validateForm() {
        boolean valid = true;

        String email = mEmailField.getText().toString();
        String required = getResources().getString(R.string.required);
        if (TextUtils.isEmpty(email)) {
            mEmailField.setError(required);
            valid = false;
        } else {
            mEmailField.setError(null);
        }

        String password = mPasswordField.getText().toString();
        if (TextUtils.isEmpty(password)) {
            mPasswordField.setError(required);
            valid = false;
        } else {
            mPasswordField.setError(null);
        }

        return valid;
    }

    //onClick button listeners
    public View.OnClickListener signInBtnListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            signIn(mEmailField.getText().toString(), mPasswordField.getText().toString());
        }
    };

    public View.OnClickListener forgotPasswordBtnListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent goToResetPassword = new Intent(LoginActivity.this, ResetPasswordActivity.class);
            startActivity(goToResetPassword);
        }
    };


    public View.OnClickListener requireAccountBtnListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent goToRequireAccount = new Intent(LoginActivity.this, RequireAccountActivity.class);
            startActivity(goToRequireAccount);
        }
    };


    private void onAuthSuccess() {        // Go to MainActivity
        startActivity(new Intent(LoginActivity.this, DashboardActivity.class));
        finish();
    }
}
