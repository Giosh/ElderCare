package it.uniba.sms.gruppo1.eldercare.SensorDetails;

import it.uniba.sms.gruppo1.eldercare.Constants.SensorsDetectionsConstants;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import it.uniba.sms.gruppo1.eldercare.R;
import it.uniba.sms.gruppo1.eldercare.SensorDetails.ItemFragment.OnListFragmentInteractionListener;
import it.uniba.sms.gruppo1.eldercare.Models.SensorDetectionModel;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static it.uniba.sms.gruppo1.eldercare.Constants.SensorsDetectionsConstants.SensorsDetectionLimits.MAX_GAS_PERCENTAGE;
import static it.uniba.sms.gruppo1.eldercare.Constants.SensorsDetectionsConstants.SensorsDetectionLimits.MAX_SMOKE_PERCENTAGE;
import static it.uniba.sms.gruppo1.eldercare.Constants.SharedPreferencesNames.LIMIT_PREFERENCES;


/**
 * {@link RecyclerView.Adapter} that can display a {@link SensorDetectionModel} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 */
public class MyItemRecyclerViewAdapter extends RecyclerView.Adapter<MyItemRecyclerViewAdapter.ViewHolder> {

    private final List<SensorDetectionModel> mValues;
    private final OnListFragmentInteractionListener mListener;
    private String sensorType;
    private Context context;

    MyItemRecyclerViewAdapter(List<SensorDetectionModel> items, OnListFragmentInteractionListener listener, String sensorType, Context context) {
        mValues = items;
        mListener = listener;
        this.sensorType = sensorType;
        this.context=context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        //It's used to store the value returnet from the function controlLimits
        boolean checkValue = false;
        //if true, the color of the value will be colored and bold
        checkValue = controlLimits(mValues.get(position).value);

        switch (mValues.get(position).sensorType) {
            case SensorsDetectionsConstants.SensorTypes.SLEEP_SENSOR:

                switch (position){
                    case 0:
                        holder.mIdView.setText(R.string.awake);
                        break;
                    case 1:
                        holder.mIdView.setText(R.string.soft_sleep);
                        break;
                    case 2:
                        holder.mIdView.setText(R.string.heavy_sleep);
                        break;
                    default:
                        holder.mIdView.setText("");
                        break;
                }
                int seconds = (int) (mValues.get(position).value / 1000) % 60 ;
                int minutes = (int) ((mValues.get(position).value / (1000*60)) % 60);
                int hours   = (int) ((mValues.get(position).value / (1000*60*60)) % 24);
                holder.mContentView.setText(String.format(Locale.getDefault(), "%d %s %d %s %d %s", hours, "h", minutes, "m", seconds, "s"));
                break;

            case SensorsDetectionsConstants.SensorTypes.CALORIES_SENSOR:
                if (checkValue){
                    holder.mContentView.setTextColor(context.getResources().getColor(R.color.orange));
                    holder.mContentView.setTypeface(null, Typeface.BOLD);
                }
                holder.mIdView.setText(R.string.burnt_calories);
                holder.mContentView.setText(String.format(Locale.ITALIAN, "%d %s", (int)mValues.get(position).value, getMeasurementUnit()));
                break;

//            case SensorsDetectionsConstants.SensorTypes.STEPS_COUNTER:
//                holder.mIdView.setText(R.string.total_steps);
//                if (checkValue){
//                    holder.mContentView.setTextColor(context.getResources().getColor(R.color.orange));
//                    holder.mContentView.setTypeface(null, Typeface.BOLD);
//                }
//                holder.mContentView.setText(String.format(Locale.ITALIAN, "%d", (int)mValues.get(position).value));
//                break;

            default:
                holder.mIdView.setText(getFormattedDate(mValues.get(position).timestamp));
                if (checkValue){
                    holder.mContentView.setTextColor(Color.RED);
                    holder.mContentView.setTypeface(null, Typeface.BOLD);
                }
                holder.mContentView.setText(String.format(Locale.ITALIAN, "%.0f %s", mValues.get(position).value, getMeasurementUnit()));
                break;

        }


        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    /**
     * This method is used to check if a value it's in the selected limits
     * @param value is the value that is going to be checked
     * @return true if the value is out of the limits, false if not
     */
    private boolean controlLimits(Double value) {
        SharedPreferences editor = context.getSharedPreferences(LIMIT_PREFERENCES, Context.MODE_PRIVATE);
        float minValue=0, maxValue=0;
        boolean checkValue = false;
        switch (sensorType) {
            case SensorsDetectionsConstants.SensorTypes.HEARTBEAT_SENSOR:
                minValue = editor.getInt(SensorsDetectionsConstants.SensorsDetectionLimits.MIN_LIMIT + SensorsDetectionsConstants.SensorTypes.HEARTBEAT_SENSOR, 0);
                maxValue = editor.getInt(SensorsDetectionsConstants.SensorsDetectionLimits.MAX_LIMIT + SensorsDetectionsConstants.SensorTypes.HEARTBEAT_SENSOR, 200);
                if (value<minValue|| value>maxValue){
                    checkValue= true;
                }
                break;

            case SensorsDetectionsConstants.SensorTypes.MOISTURE_SENSOR:
                minValue = editor.getInt(SensorsDetectionsConstants.SensorsDetectionLimits.MIN_LIMIT + SensorsDetectionsConstants.SensorTypes.MOISTURE_SENSOR, 0);
                maxValue = editor.getInt(SensorsDetectionsConstants.SensorsDetectionLimits.MAX_LIMIT + SensorsDetectionsConstants.SensorTypes.MOISTURE_SENSOR, 100);
                if (value<minValue|| value>maxValue){
                    checkValue= true;
                }
                break;

            case SensorsDetectionsConstants.SensorTypes.GAS_SENSOR:
                maxValue = MAX_GAS_PERCENTAGE;
                if(value>maxValue){
                    checkValue= true;
                }
                break;

            case SensorsDetectionsConstants.SensorTypes.SLEEP_SENSOR:
                minValue = editor.getInt(SensorsDetectionsConstants.SensorsDetectionLimits.MIN_LIMIT + SensorsDetectionsConstants.SensorTypes.SLEEP_SENSOR, 0);
                if (value<minValue){
                    checkValue= true;
                }
                break;

            case SensorsDetectionsConstants.SensorTypes.SMOKE_SENSOR:
                maxValue = MAX_SMOKE_PERCENTAGE;
                if(value>maxValue){
                    checkValue= true;
                }
                break;

            case SensorsDetectionsConstants.SensorTypes.STEPS_COUNTER:
                minValue = editor.getInt(SensorsDetectionsConstants.SensorsDetectionLimits.MIN_LIMIT + SensorsDetectionsConstants.SensorTypes.STEPS_COUNTER, 0);
                if(value<minValue){
                    checkValue= true;
                }
                break;

            case SensorsDetectionsConstants.SensorTypes.TEMPERATURE_SENSOR:
                minValue = editor.getInt(SensorsDetectionsConstants.SensorsDetectionLimits.MIN_LIMIT + SensorsDetectionsConstants.SensorTypes.TEMPERATURE_SENSOR, -50);
                maxValue = editor.getInt(SensorsDetectionsConstants.SensorsDetectionLimits.MAX_LIMIT + SensorsDetectionsConstants.SensorTypes.TEMPERATURE_SENSOR, 100);
                if (value<minValue|| value>maxValue){
                    checkValue= true;
                }
                break;

            case SensorsDetectionsConstants.SensorTypes.CALORIES_SENSOR:
                minValue = editor.getInt(SensorsDetectionsConstants.SensorsDetectionLimits.MIN_LIMIT, 0);
                if (value<minValue){
                    checkValue= true;
                }
                break;

        }
        return checkValue;

    }

    private String getMeasurementUnit() {
        switch(this.sensorType) {
            case SensorsDetectionsConstants.SensorTypes.HEARTBEAT_SENSOR:
                return "bpm";
            case SensorsDetectionsConstants.SensorTypes.CALORIES_SENSOR:
                return "KCal";
            case SensorsDetectionsConstants.SensorTypes.MOISTURE_SENSOR:
                return "%";
            case SensorsDetectionsConstants.SensorTypes.TEMPERATURE_SENSOR:
                return "°C";
            case SensorsDetectionsConstants.SensorTypes.STEPS_COUNTER:
                return "";
            case SensorsDetectionsConstants.SensorTypes.GAS_SENSOR:
                return "";
            case SensorsDetectionsConstants.SensorTypes.SMOKE_SENSOR:
                return "";
            case SensorsDetectionsConstants.SensorTypes.SLEEP_SENSOR:
                return "h";
        }

        return "";
    }

    private String getFormattedDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int hours   = calendar.get(Calendar.HOUR_OF_DAY);
        int minutes = calendar.get(Calendar.MINUTE);
        return String.format(Locale.ITALIAN, "%02d:%02d", hours, minutes);
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        final View mView;
        final TextView mIdView;
        final TextView mContentView;
        public SensorDetectionModel mItem;

        ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView =  view.findViewById(R.id.item_number);
            mContentView = view.findViewById(R.id.content);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }

}
