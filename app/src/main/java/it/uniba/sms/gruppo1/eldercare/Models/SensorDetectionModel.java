package it.uniba.sms.gruppo1.eldercare.Models;

import java.util.Date;
/*This Model reflects how sensor detections are stored in FireStore.
* Each document in the sensorsDetections inner-collection has this structure.
* This class is used to de-serialize sensor detections
 * from FireStore.
 * NOTE: this model represents Sensor Detections NOT the Sensors itself. */
public class SensorDetectionModel {
    public Date timestamp;
    public String sensorType;
    public double value;

    public SensorDetectionModel(){

    }

    public SensorDetectionModel(Date timestamp, String sensorType, double value){
        this.timestamp=timestamp;
        this.sensorType=sensorType;
        this.value=value;
    }

    public String toString(){
        StringBuilder tostring=new StringBuilder();
        tostring.append("timestamp: "+this.timestamp);
        tostring.append(" sensor type : "+this.sensorType);
        tostring.append(" value : "+this.value);
        return tostring.toString();
    }
}
