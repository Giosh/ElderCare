package it.uniba.sms.gruppo1.eldercare.SensorDetails;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.google.firebase.auth.FirebaseAuth;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

import it.uniba.sms.gruppo1.eldercare.Constants.SensorsDetectionsConstants;
import it.uniba.sms.gruppo1.eldercare.Models.SensorDetectionModel;
import it.uniba.sms.gruppo1.eldercare.R;

import static android.view.View.GONE;
import static it.uniba.sms.gruppo1.eldercare.Constants.SharedPreferencesNames.LIMIT_PREFERENCES;

/**
 * Description: this class hosts two fragments that are loaded into 2 different FrameLayouts:
 * - Chart Fragment which get data and plot it into a chart;
 * - Item Fragment which show a list of last data of a sensor.
 */
public class SensorDetailsActivity extends AppCompatActivity implements ChartFragment.OnFragmentInteractionListener,
        ItemFragment.OnListFragmentInteractionListener {

    private BottomSheetBehavior mBottomSheetBehaviour;

    static final String TAG      = "SDA";
    //static final String ARG_DATE = "date";
    public static String CURRENT_USER;


    Toolbar   toolbar;
    ImageView calendarImageView, thresholdImageView, closeImageView;
    TextView  textViewSeekBar1, textViewSeekBar2, currentDay, title;

    View      nestedScrollView;
    SeekBar   seekBar;
    Switch    switch1;
    Button    confirmButton;
    CrystalRangeSeekbar rangeSeekBar;
    ChartFragment chartFragment;
    ItemFragment itemFragment;

    private ProgressDialog mProgressDialog;


    //Variable used to get sensor type from the previous Activity
    String sensorType, date;
    //Used to store selected date.
    boolean switchValue;

    @Override
    // When we change activity, we store the date
    public void onSaveInstanceState(Bundle savedInstanceState){
        savedInstanceState.putString("data", date);
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensor_details);
        showProgressDialog();

        // [START initialize_auth]
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        // [END initialize_auth]

        if (mAuth.getCurrentUser() != null) {
            CURRENT_USER= mAuth.getCurrentUser().getEmail();
        }

        //getting sensor type from previous Activity
        sensorType = getIntent().getStringExtra("sensorType");

        toolbar               = findViewById(R.id.my_toolbar);
        calendarImageView     = findViewById(R.id.calendarImageView);
        thresholdImageView    = findViewById(R.id.thresholdImageView);
        closeImageView        = findViewById(R.id.closeImageView);
        rangeSeekBar          = findViewById(R.id.rangeSeekBar);
        seekBar               = findViewById(R.id.seekBar);
        switch1               = findViewById(R.id.switch1);
        switchValue           = false;
        textViewSeekBar1      = findViewById(R.id.textViewSeekBar1);
        textViewSeekBar2      = findViewById(R.id.textViewSeekBar2);
        confirmButton         = findViewById(R.id.button);
        currentDay            = findViewById(R.id.textView2);
        nestedScrollView      = findViewById(R.id.nestedScrollView);
        title                 = findViewById(R.id.textView3);
        mBottomSheetBehaviour = BottomSheetBehavior.from(nestedScrollView);


        // We get the date eventually from the savedInstance, if it's already saved it's useful to
        // mantain the setted date if we rotate the smartphon and the activity is re-created.
        if (savedInstanceState != null){
            update(savedInstanceState.getString("data"));
        } else {
            update(null);
        }

        // Setup back button
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        setTitle();
        setBottomSheetElements();
        setupListeners();
        loadLimitsFromSharedPreferences();
    }

    /**
     * Description: method called to show the DatePicker
     *
     * @param v View
     */
    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    /**
     * Description: when the user selects a date in the DatePickerFragment it's called this method
     * to setup data attribute contained in this class.
     *
     * @param data date in format dd/mm/yy (or eventually null)
     */
    public void update(String data) {
        Calendar dateSet     = new GregorianCalendar(TimeZone.getDefault(), Locale.getDefault());
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

        // If the data passed is null, then we assume that we want to use
        // the current date.
        if (data != null) {
            try {
                this.date = data;
                dateSet.setTime(formatter.parse(data));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else {
            // Inizializing date string
            int dayOfMonth      = dateSet.get(Calendar.DAY_OF_MONTH);
            int month           = dateSet.get(Calendar.MONTH);
            int year            = dateSet.get(Calendar.YEAR);
            this.date           = String.format("%d/%d/%d", dayOfMonth, month + 1 , year );
        }


        // Get the name of the day and then insert it in the label in the view
        String dayLongName   = dateSet.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.getDefault());
        dayLongName          = dayLongName.substring(0, 1).toUpperCase() + dayLongName.substring(1);
        currentDay.setText(dayLongName + ", " + date);

        // Reload fragments with new data replacing whatever is the content
        itemFragment  = ItemFragment.newInstance(sensorType, date);
        chartFragment = ChartFragment.newInstance(sensorType, date);
        getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout, chartFragment).commit();
        getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout2, itemFragment).commit();
    }


    @Override
    public void onFragmentInteraction(Uri uri) {}

    @Override
    public void onListFragmentInteraction(SensorDetectionModel item) {}

    public void setupListeners() {
        calendarImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SensorDetailsActivity.this.showDatePickerDialog(v);
            }
        });

        thresholdImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mBottomSheetBehaviour.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                    mBottomSheetBehaviour.setState(BottomSheetBehavior.STATE_EXPANDED);
                } else {
                    mBottomSheetBehaviour.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }
            }
        });

        // set listener
        rangeSeekBar.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                textViewSeekBar1.setText(String.valueOf(minValue));
                textViewSeekBar2.setText(String.valueOf(maxValue));
            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                int val = (progress * (seekBar.getWidth() - 2 * seekBar.getThumbOffset())) / seekBar.getMax();
                textViewSeekBar1.setText("" + progress);
                textViewSeekBar1.setX(seekBar.getX() + val + seekBar.getThumbOffset() / 2);
                //textView.setY(100); just added a value set this properly using screen with height aspect ratio , if you do not set it
                // by default it will be there below seek bar
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });


        switch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                switchValue = isChecked;
            }
        });

        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveLimitsToSharedPreferences();
                mBottomSheetBehaviour.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        });

        closeImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetBehaviour.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        });
    }

    public void saveLimitsToSharedPreferences() {
        SharedPreferences.Editor editor = getSharedPreferences(LIMIT_PREFERENCES, MODE_PRIVATE).edit();

        switch (sensorType) {
            case SensorsDetectionsConstants.SensorTypes.HEARTBEAT_SENSOR:
                editor.putInt(SensorsDetectionsConstants.SensorsDetectionLimits.MIN_LIMIT + SensorsDetectionsConstants.SensorTypes.HEARTBEAT_SENSOR,
                        Integer.parseInt(textViewSeekBar1.getText().toString()));
                editor.putInt(SensorsDetectionsConstants.SensorsDetectionLimits.MAX_LIMIT + SensorsDetectionsConstants.SensorTypes.HEARTBEAT_SENSOR,
                        Integer.parseInt(textViewSeekBar2.getText().toString()));
                break;

            case SensorsDetectionsConstants.SensorTypes.MOISTURE_SENSOR:
                editor.putInt(SensorsDetectionsConstants.SensorsDetectionLimits.MIN_LIMIT + SensorsDetectionsConstants.SensorTypes.MOISTURE_SENSOR,
                        Integer.parseInt(textViewSeekBar1.getText().toString()));
                editor.putInt(SensorsDetectionsConstants.SensorsDetectionLimits.MAX_LIMIT + SensorsDetectionsConstants.SensorTypes.MOISTURE_SENSOR,
                        Integer.parseInt(textViewSeekBar2.getText().toString()));
                break;

            case SensorsDetectionsConstants.SensorTypes.GAS_SENSOR:
                editor.putBoolean(SensorsDetectionsConstants.SensorsDetectionLimits.MAX_LIMIT + SensorsDetectionsConstants.SensorTypes.GAS_SENSOR, switchValue);
                break;

            case SensorsDetectionsConstants.SensorTypes.SLEEP_SENSOR:
                editor.putInt(SensorsDetectionsConstants.SensorsDetectionLimits.MIN_LIMIT + SensorsDetectionsConstants.SensorTypes.SLEEP_SENSOR, Integer.parseInt(textViewSeekBar1.getText()
                        .toString()));
                break;

            case SensorsDetectionsConstants.SensorTypes.SMOKE_SENSOR:
                editor.putBoolean(SensorsDetectionsConstants.SensorsDetectionLimits.MAX_LIMIT + SensorsDetectionsConstants.SensorTypes.SMOKE_SENSOR, switchValue);
                break;

            case SensorsDetectionsConstants.SensorTypes.STEPS_COUNTER:
                editor.putInt(SensorsDetectionsConstants.SensorsDetectionLimits.MIN_LIMIT + SensorsDetectionsConstants.SensorTypes.STEPS_COUNTER, Integer.parseInt(textViewSeekBar1.getText()
                        .toString()));
                break;

            case SensorsDetectionsConstants.SensorTypes.TEMPERATURE_SENSOR:
                editor.putInt(SensorsDetectionsConstants.SensorsDetectionLimits.MIN_LIMIT + SensorsDetectionsConstants.SensorTypes.TEMPERATURE_SENSOR, Integer.parseInt(textViewSeekBar1.getText()
                        .toString()));
                editor.putInt(SensorsDetectionsConstants.SensorsDetectionLimits.MAX_LIMIT + SensorsDetectionsConstants.SensorTypes.TEMPERATURE_SENSOR, Integer.parseInt(textViewSeekBar2.getText()
                        .toString()));
                break;

            case SensorsDetectionsConstants.SensorTypes.CALORIES_SENSOR:
                editor.putInt(SensorsDetectionsConstants.SensorsDetectionLimits.MIN_LIMIT, Integer.parseInt(textViewSeekBar1.getText()
                        .toString()));
                break;
        }
        editor.apply();
    }

    public void loadLimitsFromSharedPreferences() {
        SharedPreferences editor = getSharedPreferences(LIMIT_PREFERENCES, MODE_PRIVATE);
        int minValue, maxValue;
        boolean switchStatus;

        switch (sensorType) {
            case SensorsDetectionsConstants.SensorTypes.HEARTBEAT_SENSOR:
                minValue = editor.getInt(SensorsDetectionsConstants.SensorsDetectionLimits.MIN_LIMIT + SensorsDetectionsConstants.SensorTypes.HEARTBEAT_SENSOR, 0);
                maxValue = editor.getInt(SensorsDetectionsConstants.SensorsDetectionLimits.MAX_LIMIT + SensorsDetectionsConstants.SensorTypes.HEARTBEAT_SENSOR, 200);
                textViewSeekBar1.setText(String.valueOf(minValue));
                textViewSeekBar2.setText(String.valueOf(maxValue));
                rangeSeekBar.setMinStartValue(minValue);
                rangeSeekBar.setMaxStartValue(maxValue);
                rangeSeekBar.apply();
                break;

            case SensorsDetectionsConstants.SensorTypes.MOISTURE_SENSOR:
                minValue = editor.getInt(SensorsDetectionsConstants.SensorsDetectionLimits.MIN_LIMIT + SensorsDetectionsConstants.SensorTypes.MOISTURE_SENSOR, 0);
                maxValue = editor.getInt(SensorsDetectionsConstants.SensorsDetectionLimits.MAX_LIMIT + SensorsDetectionsConstants.SensorTypes.MOISTURE_SENSOR, 100);
                textViewSeekBar1.setText(String.valueOf(minValue));
                textViewSeekBar2.setText(String.valueOf(maxValue));
                rangeSeekBar.setMinStartValue(minValue);
                rangeSeekBar.setMaxStartValue(maxValue);
                rangeSeekBar.apply();
                break;

            case SensorsDetectionsConstants.SensorTypes.GAS_SENSOR:
                switchStatus = editor.getBoolean(SensorsDetectionsConstants.SensorsDetectionLimits.MAX_LIMIT + SensorsDetectionsConstants.SensorTypes.GAS_SENSOR, false);
                switch1.setChecked(switchStatus);
                break;

            case SensorsDetectionsConstants.SensorTypes.SLEEP_SENSOR:
                minValue = editor.getInt(SensorsDetectionsConstants.SensorsDetectionLimits.MIN_LIMIT + SensorsDetectionsConstants.SensorTypes.SLEEP_SENSOR, 0);
                textViewSeekBar1.setText(String.valueOf(minValue));
                seekBar.setProgress(minValue);
                break;

            case SensorsDetectionsConstants.SensorTypes.SMOKE_SENSOR:
                switchStatus = editor.getBoolean(SensorsDetectionsConstants.SensorsDetectionLimits.MAX_LIMIT + SensorsDetectionsConstants.SensorTypes.SMOKE_SENSOR, false);
                switch1.setChecked(switchStatus);
                break;

            case SensorsDetectionsConstants.SensorTypes.STEPS_COUNTER:
                minValue = editor.getInt(SensorsDetectionsConstants.SensorsDetectionLimits.MIN_LIMIT + SensorsDetectionsConstants.SensorTypes.STEPS_COUNTER, 0);
                textViewSeekBar1.setText(String.valueOf(minValue));
                seekBar.setProgress(minValue);
                break;

            case SensorsDetectionsConstants.SensorTypes.TEMPERATURE_SENSOR:
                minValue = editor.getInt(SensorsDetectionsConstants.SensorsDetectionLimits.MIN_LIMIT + SensorsDetectionsConstants.SensorTypes.TEMPERATURE_SENSOR, -50);
                maxValue = editor.getInt(SensorsDetectionsConstants.SensorsDetectionLimits.MAX_LIMIT + SensorsDetectionsConstants.SensorTypes.TEMPERATURE_SENSOR, 100);
                textViewSeekBar1.setText(String.valueOf(minValue));
                textViewSeekBar2.setText(String.valueOf(maxValue));
                rangeSeekBar.setMinStartValue(minValue);
                rangeSeekBar.setMaxStartValue(maxValue);
                rangeSeekBar.apply();
                break;

            case SensorsDetectionsConstants.SensorTypes.CALORIES_SENSOR:
                minValue = editor.getInt(SensorsDetectionsConstants.SensorsDetectionLimits.MIN_LIMIT, 0);
                textViewSeekBar1.setText(String.valueOf(minValue));
                seekBar.setProgress(minValue);
                break;
        }
    }

    public void setTitle() {
        switch (sensorType) {
            case SensorsDetectionsConstants.SensorTypes.HEARTBEAT_SENSOR:
                toolbar.setTitle(getString(R.string.title_heartbeat));
                break;

            case SensorsDetectionsConstants.SensorTypes.MOISTURE_SENSOR:
                toolbar.setTitle(getString(R.string.title_moisture));
                break;

            case SensorsDetectionsConstants.SensorTypes.GAS_SENSOR:
                toolbar.setTitle(getString(R.string.title_gas));
                break;

            case SensorsDetectionsConstants.SensorTypes.SLEEP_SENSOR:
                toolbar.setTitle(getString(R.string.title_sleep));
                break;

            case SensorsDetectionsConstants.SensorTypes.SMOKE_SENSOR:
                toolbar.setTitle(getString(R.string.title_smoke));
                break;

            case SensorsDetectionsConstants.SensorTypes.STEPS_COUNTER:
                toolbar.setTitle(getString(R.string.title_steps));
                break;

            case SensorsDetectionsConstants.SensorTypes.TEMPERATURE_SENSOR:
                toolbar.setTitle(getString(R.string.title_temperature));
                break;

            case SensorsDetectionsConstants.SensorTypes.CALORIES_SENSOR:
                toolbar.setTitle(getString(R.string.title_calories));
                break;
        }
    }


    public void setBottomSheetElements() {
        // Title will change in case of sleep / steps, it will be a goal and not a threshold
        switch (sensorType) {
            case SensorsDetectionsConstants.SensorTypes.HEARTBEAT_SENSOR:
                seekBar.setVisibility(GONE);
                rangeSeekBar.setMinValue(0);
                rangeSeekBar.setMaxValue(200);
                textViewSeekBar1.setText(String.valueOf(0));
                textViewSeekBar2.setText(String.valueOf(200));
                switch1.setVisibility(GONE);
                break;

            case SensorsDetectionsConstants.SensorTypes.MOISTURE_SENSOR:
                seekBar.setVisibility(GONE);
                rangeSeekBar.setMinValue(0);
                rangeSeekBar.setMaxValue(100);
                textViewSeekBar1.setText(String.valueOf(0));
                textViewSeekBar2.setText(String.valueOf(100));
                switch1.setVisibility(GONE);
                break;

            case SensorsDetectionsConstants.SensorTypes.GAS_SENSOR:
                rangeSeekBar.setVisibility(GONE);
                seekBar.setVisibility(GONE);
                title.setText(R.string.costumize);
                textViewSeekBar1.setVisibility(GONE);
                textViewSeekBar2.setVisibility(GONE);
                break;

            case SensorsDetectionsConstants.SensorTypes.SLEEP_SENSOR:
                rangeSeekBar.setVisibility(GONE);
                seekBar.setMax(10);
                title.setText(R.string.set_goal);
                textViewSeekBar1.setText(String.valueOf(0)); //max 10
                textViewSeekBar2.setVisibility(GONE);
                switch1.setVisibility(GONE);
                break;

            case SensorsDetectionsConstants.SensorTypes.SMOKE_SENSOR:
                seekBar.setVisibility(GONE);
                rangeSeekBar.setVisibility(GONE);
                title.setText(R.string.costumize);
                textViewSeekBar1.setVisibility(GONE);
                textViewSeekBar2.setVisibility(GONE);
                break;

            case SensorsDetectionsConstants.SensorTypes.STEPS_COUNTER:
                rangeSeekBar.setVisibility(GONE);
                seekBar.setMax(20000);
                title.setText(R.string.set_goal);
                textViewSeekBar1.setText(String.valueOf(0)); //max 20000
                textViewSeekBar2.setVisibility(GONE);
                switch1.setVisibility(GONE);
                break;

            case SensorsDetectionsConstants.SensorTypes.TEMPERATURE_SENSOR:
                seekBar.setVisibility(GONE);
                rangeSeekBar.setMinValue(-50);
                rangeSeekBar.setMaxValue(100);
                textViewSeekBar1.setText(String.valueOf(-50));
                textViewSeekBar2.setText(String.valueOf(100));
                switch1.setVisibility(GONE);
                break;

            case SensorsDetectionsConstants.SensorTypes.CALORIES_SENSOR:
                rangeSeekBar.setVisibility(GONE);
                seekBar.setMax(5000);
                title.setText(R.string.set_goal);
                textViewSeekBar1.setText(String.valueOf(0)); //max 5000
                textViewSeekBar2.setVisibility(GONE);
                switch1.setVisibility(GONE);
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()) {
            case android.R.id.home:
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                    TaskStackBuilder.create(this)
                            .addNextIntentWithParentStack(upIntent)
                            .startActivities();
                } else {
                    NavUtils.navigateUpTo(this, upIntent);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(true);
        }
        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }


}
