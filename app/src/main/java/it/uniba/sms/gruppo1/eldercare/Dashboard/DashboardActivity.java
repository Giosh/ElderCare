package it.uniba.sms.gruppo1.eldercare.Dashboard;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.DateFormat;
import java.util.ArrayList;

import it.uniba.sms.gruppo1.eldercare.Constants.CollectionNames;
import it.uniba.sms.gruppo1.eldercare.Constants.SensorsDetectionsConstants;
import it.uniba.sms.gruppo1.eldercare.Models.ProfileInformationModel;
import it.uniba.sms.gruppo1.eldercare.Notifications.NotificationsService;
import it.uniba.sms.gruppo1.eldercare.RegistrationActivities.LoginActivity;
import it.uniba.sms.gruppo1.eldercare.R;
import it.uniba.sms.gruppo1.eldercare.Models.SensorDetectionModel;

public class DashboardActivity extends AppCompatActivity implements DashboardBaseFragment.OnFragmentInteractionListener, View.OnClickListener,
        DashboardElderFragment.ElderDashboardElementsUpdater, DashboardHomeFragment.HomeDashboardElementsUpdater {

    private static final String SELECTED_ITEM = "arg_selected_item";
    private BottomNavigationView mBottomNav;

    TextView heartbeatValue;
    TextView sleepValue;
    TextView caloriesValue;
    TextView stepsValue;

    TextView temperatureValue;
    TextView moistureValue;
    TextView smokeValue;
    TextView gasValue;

    MenuItem prevMenuItem = null;

    private CollectionReference mElderProfileInformation;
    private FirebaseFirestore mDatabase;

    boolean doubleBackToExitPressedOnce = false;

    /**
     * The number of pages to show.
     */
    private static final int NUM_PAGES = 2;

    /**
     * The pager widget, which handles animation and allows swiping horizontally to access previous
     * and next wizard steps.
     */
    private ViewPager mPager;

    /**
     * The pager adapter, which provides the pages to the view pager widget.
     */
    private PagerAdapter mPagerAdapter;

    private final static String TAG=DashboardActivity.class.getName();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        mDatabase = FirebaseFirestore.getInstance();

        // Instantiate a ViewPager and a PagerAdapter.
        mPager =  findViewById(R.id.pager);
        mPagerAdapter = new DashboardActivity.ScreenSlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);

        // This listener is used to change the Bottom Navigation Bar changes when sliding between fragments
        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}
            @Override
            public void onPageScrollStateChanged(int state) {}

            @Override
            public void onPageSelected(int position) {
                if (prevMenuItem != null) {
                    prevMenuItem.setChecked(false);
                }
                else {
                    mBottomNav.getMenu().getItem(0).setChecked(false);
                }
                setTitleWithId(mBottomNav.getMenu().getItem(position).getItemId());
                mBottomNav.getMenu().getItem(position).setChecked(true);
                prevMenuItem = mBottomNav.getMenu().getItem(position);
            }

        });

        // Instantiate a Bottom Navigation bar
        mBottomNav = findViewById(R.id.navigation);
        mBottomNav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                if(item.getItemId() == R.id.navigation_elder){
                    mPager.setCurrentItem(0, true);
                }
                if(item.getItemId() == R.id.navigation_home) {
                    mPager.setCurrentItem(1, true);
                }
                return true;
            }
        });

        if (savedInstanceState!=null) {
            setTitleWithId(savedInstanceState.getInt(SELECTED_ITEM, 0));
        }
        else{
            mPager.setCurrentItem(R.id.navigation_elder);
        }

        //This method is used to set the elder name on the title of the bottom navigation view if present on the database
        setElderName();

       Intent serviceIntent=new Intent(this, NotificationsService.class);
       startService(serviceIntent);
    }


    private void setElderName (){

        //Linking the variable with the path on the database where are present the profile information
        mElderProfileInformation = mDatabase.collection(CollectionNames.ELDERS_COLLECTION_NAME).document(LoginActivity.CURRENT_USER).
                collection(CollectionNames.PROFILE_INFORMATIONS);

        mElderProfileInformation.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(QuerySnapshot queryDocumentSnapshots,FirebaseFirestoreException e) {
                mElderProfileInformation.orderBy("lastUpdate").limit(1)
                        .get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        if (!queryDocumentSnapshots.isEmpty()) {
                            ProfileInformationModel profile = new ProfileInformationModel(queryDocumentSnapshots.toObjects(ProfileInformationModel.class));
                            //Setting the Bottom Navigation Bar title with the name selected on profile, if exists
                            mBottomNav.getMenu().getItem(0).setTitle(profile.elderName);
                        }
                    }
                });
            }
        });
    }


    /**
     * This method is used to save the fragment selected before the rotation in order to show it again
     * after the screen rotation
     * @param outState contains the saved information
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(SELECTED_ITEM, mBottomNav.getSelectedItemId());
    }

    /**
     * this method is used to set the correct title in the onCreate
     * @param menuId is the selected id
     */
    private void setTitleWithId(int menuId){
            if(menuId == 0|| menuId == R.id.navigation_elder){
                setTitle( R.string.title_elder);
            }
            if (menuId == R.id.navigation_home){
                setTitle( R.string.title_home);
            }
    }

    /**
     * This method is used to avoid unwanted exits from the application
     * On first back pressed it makes a toast that say to the used to click another time to exit
     */
    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;

        Toast.makeText(DashboardActivity.this, R.string.exit_warning,
                Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }


    @Override
    public void onFragmentInteraction(Uri uri) {}

    /**
     * this method is used to handle the three dot menu options
     * @param item corrispond to the selected item id
     * @return the item
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.profile:
                startActivity(new Intent(DashboardActivity.this, ProfileActivity.class));
                break;
            case R.id.sign_out:
                FirebaseAuth.getInstance().signOut();
                startActivity(new Intent(this, LoginActivity.class));
                finish();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * this method is used to inflate into the layout the menu
     * @param menu is the menu to inflate into the toolbar
     * @return the menu
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.action_bar_options, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onClick(View view) {}

    /**
     * This method is used to update and set the latest sensor detections every time that new
     * elder-related detections arrive.
     * @param lastSensorsDetections is an array of the last detections received on FireStore
     */
    @Override
    public void updateElderDashboardUI(ArrayList<SensorDetectionModel> lastSensorsDetections) {
        heartbeatValue  = findViewById(R.id.heartbeat_view);
        sleepValue      = findViewById(R.id.sleep_view);
        caloriesValue   = findViewById(R.id.calories_view);
        stepsValue      = findViewById(R.id.steps_count_view);

        String valueNotAvailable = getResources().getString(R.string.value_not_available);
        if (lastSensorsDetections != null){
            for (SensorDetectionModel s: lastSensorsDetections){
                try {
                    switch (s.sensorType){
                        case SensorsDetectionsConstants.SensorTypes.HEARTBEAT_SENSOR:
                            if(heartbeatValue!=null){
                                heartbeatValue.setText(String.valueOf((int) s.value));
                                break;
                            }else
                                break;
                        case SensorsDetectionsConstants.SensorTypes.SLEEP_SENSOR:
                            if(sleepValue!=null){
                                switch ((int) s.value){
                                    case SensorsDetectionsConstants.SleepSensorConstants.AWAKE:
                                        sleepValue.setText(getString(R.string.awake));
                                        break;
                                    case SensorsDetectionsConstants.SleepSensorConstants.SOFT_SLEEP:
                                        sleepValue.setText(getString(R.string.soft_sleep));
                                        break;
                                    case SensorsDetectionsConstants.SleepSensorConstants.HEAVY_SLEEP:
                                        sleepValue.setText(getString(R.string.heavy_sleep));
                                        break;
                                    default:
                                        break;
                                }
                            }else
                                break;
                        case SensorsDetectionsConstants.SensorTypes.STEPS_COUNTER:
                            if(stepsValue!=null){
                                stepsValue.setText(String.valueOf((int) s.value));
                                break;
                            }else
                                break;
                        case SensorsDetectionsConstants.SensorTypes.CALORIES_SENSOR:
                            if(caloriesValue!=null){
                                caloriesValue.setText(String.valueOf((int) s.value));
                                break;
                            }else
                                break;
                    }
                }catch (NullPointerException exc){
                    Log.d(TAG,"AAAA "+exc.getMessage());
                }
            }
        }
    }


    /**
     * This method is used to update and set the latest sensor detections every time that new
     * house-related detections arrive.
     * @param lastSensorsDetections is an array of the last detections received on FireStore
     */
    @Override
    public void updateHomeDashboardUI(ArrayList<SensorDetectionModel> lastSensorsDetections) {
        temperatureValue = findViewById(R.id.temperature_view);
        moistureValue    = findViewById(R.id.moisture_view);
        smokeValue       = findViewById(R.id.smoke_view);
        gasValue         = findViewById(R.id.gas_view);

        String valueNotAvailable = getResources().getString(R.string.value_not_available);

        if (lastSensorsDetections!=null){
            for (SensorDetectionModel s: lastSensorsDetections){
                try {
                    switch (s.sensorType){
                        case SensorsDetectionsConstants.SensorTypes.SMOKE_SENSOR:
                            if(smokeValue!=null){
                                smokeValue.setText(String.valueOf(s.value));
                                break;
                            }else
                                break;
                        case SensorsDetectionsConstants.SensorTypes.GAS_SENSOR:
                            if(gasValue!=null){
                                gasValue.setText(String.valueOf(s.value));
                                break;
                            }else
                                break;
                        case SensorsDetectionsConstants.SensorTypes.TEMPERATURE_SENSOR:
                            if(temperatureValue!=null){
                                temperatureValue.setText(String.valueOf((int) s.value));
                                break;
                            }else
                                break;
                        case SensorsDetectionsConstants.SensorTypes.MOISTURE_SENSOR:
                            if(moistureValue!=null){
                                moistureValue.setText(String.valueOf(s.value));
                                break;
                            }else
                                break;
                    }
                }catch (NullPointerException exc){
                    Log.d(TAG,"AAAA "+exc.getMessage());
                }

            }
        }

    }

    /**
     * This class is used from the ViewPager to select the fragment when sliding
     */
    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        private ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            if (position == 1){
                setTitleWithId(R.id.navigation_home);
                mBottomNav.setSelectedItemId(R.id.navigation_home);
                return new DashboardHomeFragment();
            }
            else{
                setTitleWithId(R.id.navigation_elder);
                mBottomNav.setSelectedItemId(R.id.navigation_elder);
                return new DashboardElderFragment();
            }
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }

}
