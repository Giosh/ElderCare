package it.uniba.sms.gruppo1.eldercare.Constants;
/*These constants represents the names of the collections stored in FireStore.
* Elders is the main collection: for each account identified by the same e-mail given for the login,
* this collection register all the information related to the elder supervised by the pearson the
* account belongs to.
* sensorsDetections is the inner-collection that contains all the detections coming from
* all the registered sensors, for the given user.
* profileInfo is an inner-field which contains general information related to the supervised
* elder, saved by the user*/
public final class CollectionNames {
    public static final String ELDERS_COLLECTION_NAME="elders";
    public static final String SENSORS_DETECTIONS="sensorsDetections";
    public static final String PROFILE_INFORMATIONS="profileInfo";
}
