package it.uniba.sms.gruppo1.eldercare.SensorDetails;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import it.uniba.sms.gruppo1.eldercare.Constants.CollectionNames;
import it.uniba.sms.gruppo1.eldercare.Constants.SensorsDetectionsConstants;
import it.uniba.sms.gruppo1.eldercare.RegistrationActivities.LoginActivity;
import it.uniba.sms.gruppo1.eldercare.Models.SensorDetectionModel;
import it.uniba.sms.gruppo1.eldercare.R;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;


/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class ItemFragment extends Fragment {
    //constants used when putting and getting extras from Bundle
    private static final String ARG_SENSOR_TYPE = "sensorType";
    private static final String ARG_DATE        = "date";

    final String TAG = "Item Fragment";
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;

    private CollectionReference mElderSensorsDetections;

    private String sensorType;
    private String date;
    private Calendar cal;
    private Timestamp endOfTheDay;
    private Timestamp startOfTheDay;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ItemFragment() {}

    public static ItemFragment newInstance(String sensorType, String date) {
        ItemFragment fragment = new ItemFragment();
        Bundle args = new Bundle();

        args.putString("sensorType", sensorType);
        args.putString("date", date);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        FirebaseFirestore mDatabase = FirebaseFirestore.getInstance();
        mElderSensorsDetections = mDatabase.collection(CollectionNames.ELDERS_COLLECTION_NAME).document(SensorDetailsActivity.CURRENT_USER).
                collection(CollectionNames.SENSORS_DETECTIONS);

        final View view = inflater.inflate(R.layout.fragment_item_list, container, false);

        if (getArguments() != null) {

            this.cal        = Calendar.getInstance();
            this.sensorType = getArguments().getString(ARG_SENSOR_TYPE);
            this.date       = getArguments().getString(ARG_DATE);

            // Inizialize the calendar, in order to set the properties for the filter
            try {
                cal.setTime(formatter.parse(date));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            // The startOfTheDay is inizialized with the selected day at 00:00:00
            this.startOfTheDay = new Timestamp(cal.getTime());

            // The endOfTheDay is inizialized with the selected day but at 23.59.59
            cal.set(Calendar.HOUR_OF_DAY, 23);
            cal.set(Calendar.MINUTE,      59);
            cal.set(Calendar.SECOND,      59);
            this.endOfTheDay = new Timestamp(cal.getTime());
        }


        mElderSensorsDetections.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(QuerySnapshot queryDocumentSnapshots,FirebaseFirestoreException e) {
                mElderSensorsDetections.whereEqualTo("sensorType", sensorType)
                          .whereGreaterThanOrEqualTo("timestamp", startOfTheDay)
                          .whereLessThanOrEqualTo("timestamp", endOfTheDay)
                          .orderBy("timestamp")
                          .get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                            @Override
                            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {

                                ArrayList<SensorDetectionModel> dataToDisplay = new ArrayList<>(queryDocumentSnapshots.toObjects(SensorDetectionModel.class));
                                if(!dataToDisplay.isEmpty()) {

                                    switch(sensorType) {
                                        case SensorsDetectionsConstants.SensorTypes.SLEEP_SENSOR:
                                            dataToDisplay = getSleepTime(dataToDisplay);
                                            break;

                                        case SensorsDetectionsConstants.SensorTypes.CALORIES_SENSOR:
                                        //case SensorsDetectionsConstants.SensorTypes.STEPS_COUNTER:
                                            dataToDisplay = getDataSum(dataToDisplay, sensorType);

                                        default:

                                            Collections.reverse(dataToDisplay);
                                            break;

                                    }


                                    // Set the adapter
                                    if (view instanceof RecyclerView) {
                                        Context context = view.getContext();
                                        RecyclerView recyclerView = (RecyclerView) view;
                                        if (mColumnCount <= 1) {
                                            recyclerView.setLayoutManager(new LinearLayoutManager(context));
                                        } else {
                                            recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
                                        }
                                        recyclerView.setAdapter(new MyItemRecyclerViewAdapter(dataToDisplay, mListener, sensorType, context));
                                    }
                                }
                            }
                          });
                }
            });

        return view;
    }



    public ArrayList<SensorDetectionModel> getSleepTime(ArrayList<SensorDetectionModel> dataToDisplay) {
        double sum[] = new double[3];

        for(int i=1; i<dataToDisplay.size(); i++) {

            double t1 = dataToDisplay.get(i-1).timestamp.getTime();
            double t2 = dataToDisplay.get(i).timestamp.getTime();
            double interval = t2-t1;
            Log.d(TAG, String.valueOf(interval));

            switch ((int) dataToDisplay.get(i).value) {
                case 0:
                    sum [0] += interval;
                    break;

                case 1:
                    sum [1] += interval;
                    break;

                case 2:
                    sum [2] += interval;
                    break;
            }
        }

        dataToDisplay = new ArrayList<>();
        dataToDisplay.add(new SensorDetectionModel(
                new Date(),
                SensorsDetectionsConstants.SensorTypes.SLEEP_SENSOR,
                sum[0]
        ));
        dataToDisplay.add(new SensorDetectionModel(
                new Date(),
                SensorsDetectionsConstants.SensorTypes.SLEEP_SENSOR,
                sum[1]
        ));
        dataToDisplay.add(new SensorDetectionModel(
                new Date(),
                SensorsDetectionsConstants.SensorTypes.SLEEP_SENSOR,
                sum[2]
        ));

        return dataToDisplay;
    }


    public ArrayList<SensorDetectionModel> getDataSum(ArrayList<SensorDetectionModel> dataToDisplay, String sensorType) {
        int sum = 0;

        for(SensorDetectionModel s: dataToDisplay) {
            sum += s.value;
        }

        dataToDisplay = new ArrayList<>();
        dataToDisplay.add(new SensorDetectionModel(
                new Date(),
                sensorType,
                sum
        ));

        return dataToDisplay;

    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(SensorDetectionModel item);
    }


}
