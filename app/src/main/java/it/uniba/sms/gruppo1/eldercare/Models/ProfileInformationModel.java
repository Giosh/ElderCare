package it.uniba.sms.gruppo1.eldercare.Models;

import java.util.Date;
import java.util.List;

public class ProfileInformationModel {
    public String elderName;
    public String address;
    public Date birthDate;
    public String phoneOne;
    public String phoneTwo;

    //This value is used because Firebase doesn't consent to order in a descending order
    //so in order to retrive the latest profile information this value is
    //initialized with -1 * (new Date().getTime()).
    //In this way when in an ORDER BY lastupdate query the most recent one will be the first
    public long lastUpdate;

    public ProfileInformationModel(){

    }

    public ProfileInformationModel(String elderName, String address, Date birthDate, String phoneOne, String phoneTwo) {
        this.elderName  = elderName;
        this.address    = address;
        this.birthDate  = birthDate;
        this.phoneOne   = phoneOne;
        this.phoneTwo   = phoneTwo;
        this.lastUpdate = -1 * (new Date().getTime());

    }

    public ProfileInformationModel(List<ProfileInformationModel> profileInformationModels) {
        this.address    = profileInformationModels.get(0).address;
        this.elderName  = profileInformationModels.get(0).elderName;
        this.birthDate  = profileInformationModels.get(0).birthDate;
        this.phoneOne   = profileInformationModels.get(0).phoneOne;
        this.phoneTwo   = profileInformationModels.get(0).phoneTwo;
        this.lastUpdate = -1 * (new Date().getTime());
    }
}