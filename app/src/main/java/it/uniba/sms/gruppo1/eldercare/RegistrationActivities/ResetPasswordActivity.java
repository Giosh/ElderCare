package it.uniba.sms.gruppo1.eldercare.RegistrationActivities;

import android.support.annotation.NonNull;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

import it.uniba.sms.gruppo1.eldercare.R;


public class ResetPasswordActivity extends BaseActivity {

    private EditText inputEmail;
    private FirebaseAuth auth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_reset_password);

        inputEmail  =  findViewById(R.id.email);

        Button btnReset    =  findViewById(R.id.btn_reset_password);


        auth = FirebaseAuth.getInstance();

        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String email = inputEmail.getText().toString().trim();

                if (TextUtils.isEmpty(email)) {
                    Toast.makeText(getApplication(), R.string.insert_email, Toast.LENGTH_SHORT).show();
                    return;
                }

                showProgressDialog();

                auth.sendPasswordResetEmail(email)

                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    Toast.makeText(ResetPasswordActivity.this, R.string.recovery_mail_success, Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(ResetPasswordActivity.this, R.string.recovery_mail_fail, Toast.LENGTH_SHORT).show();
                                }
                                hideProgressDialog();
                            }
                        });
            }
        });
    }

}