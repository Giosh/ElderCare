package it.uniba.sms.gruppo1.eldercare.Notifications;

import android.content.SharedPreferences;
import android.util.Log;

import it.uniba.sms.gruppo1.eldercare.Constants.SensorsDetectionsConstants;
import it.uniba.sms.gruppo1.eldercare.Models.SensorDetectionModel;

import static it.uniba.sms.gruppo1.eldercare.Notifications.NotificationsService.TAG_DI_QUESTO_TEST;
/*
* This class helps to keep all the methods needed for limit checking in one single place,
* instead of simply adding them in the service class. This helps to keep the code simpler
* and better organized. It contains all the necessary to check all the detection values, except
* for the sleep detections.
* */

public class LimitsChecker {

    public static final String NO_NOTIFICATION="no_notification";
    public static final String TAG="LIMITS CHECKER ";
    //Elder Limits
    private static int minHeartBeatLimit;
    private static int maxHeartBeatLimit;
    //minStepsLimit actually stands for a goal to be achieved, but we decided to keep it this way to
    // maintain the code more coherent. The same for minCaloriesLimit;
    private static int minStepsLimit;
    private static int minCaloriesLimit;

    //Home Limits
    private static int minTemperatureLimit;
    private static int maxTemperatureLimit;
    private static int maxMoistureLimit;
    private static int minMoistureLimit;
    //GasLimits and SmokeLimits are not actually settable. The user can only specify whether
    //he or she wants to be notified when smoke and gas are detected in his or her elder's house,
    // that is, when the percentage of smoke or gas concentration passes the maximum safe value.
    //This is why these limits boolean values.
    private static boolean maxGasLimit;
    private static boolean maxSmokeLimit;

//if the type of this incoming detection belongs to the Elder personal values, then this is the
// function to be called. We created two separate functions for elder and house values, to keep the
// code more organized.
    public String checkThisElderDetection(SensorDetectionModel detection, SharedPreferences limitsSharedPreferences){
        //before we perform the check we load the limits from the sharedPreferences, if the result value
        //retrieved from the shared preferences for a given sensor type limit is 0, then we
        //consider that limit not set, and we do not perform the check for the detection value,
        //for that limit.
        initElderLimits(limitsSharedPreferences);
        switch (detection.sensorType){
            case SensorsDetectionsConstants.SensorTypes.HEARTBEAT_SENSOR:
                if(maxHeartBeatLimit==0&& minHeartBeatLimit==0)
                    return NO_NOTIFICATION;
                else if(maxHeartBeatLimit==0&& minHeartBeatLimit!=0)
                    return checkMinHeartbeatLimit((int)detection.value);
                else if(maxHeartBeatLimit!=0&& minHeartBeatLimit==0)
                    return checkMaxHeartbeatLimit((int)detection.value);
                else if (maxHeartBeatLimit!=0 && minHeartBeatLimit!=0)
                    return checkHeartbeatLimit((int) detection.value);
            case SensorsDetectionsConstants.SensorTypes.CALORIES_SENSOR:
                if(minCaloriesLimit!=0)
                    return checkCaloriesLimit((int) detection.value);
                else return NO_NOTIFICATION;

            case SensorsDetectionsConstants.SensorTypes.STEPS_COUNTER:
                if(minStepsLimit!=0)
                    return checkStepsLimit((int)detection.value);
                else return NO_NOTIFICATION;
            default:
                return NO_NOTIFICATION;

        }

    }

    public String checkThisHomeDetection(SensorDetectionModel detection, SharedPreferences limitsSharedPreferences){
        initHomeLimits(limitsSharedPreferences);
        switch (detection.sensorType){
            case SensorsDetectionsConstants.SensorTypes.TEMPERATURE_SENSOR:
                if(maxTemperatureLimit==0&& minTemperatureLimit==0)
                    return NO_NOTIFICATION;
                else if(maxTemperatureLimit==0&& minTemperatureLimit!=0)
                    return checkMinTemperatureLimit((int)detection.value);
                else if(maxHeartBeatLimit!=0&& minHeartBeatLimit==0)
                    return checkMaxTemperatureLimit((int)detection.value);
                else if (maxTemperatureLimit!=0 && minTemperatureLimit!=0)
                    return checkTemperatureLimit((int) detection.value);
            case SensorsDetectionsConstants.SensorTypes.SMOKE_SENSOR:
                if(maxSmokeLimit)
                    return checkSmokeLimit((int) detection.value);
                else
                    return NO_NOTIFICATION;
            case SensorsDetectionsConstants.SensorTypes.GAS_SENSOR:
                if(maxGasLimit)
                    return checkGasLimit((int) detection.value);
                else return NO_NOTIFICATION;

            case SensorsDetectionsConstants.SensorTypes.MOISTURE_SENSOR:
                if(maxMoistureLimit==0&& minMoistureLimit==0)
                    return NO_NOTIFICATION;
                else if(maxMoistureLimit==0&& minMoistureLimit!=0)
                    return checkMinMoistureLimit((int)detection.value);
                else if(maxMoistureLimit!=0&& minMoistureLimit==0)
                    return checkMaxMoistureLimit((int)detection.value);
                else if (maxMoistureLimit!=0 && minMoistureLimit!=0)
                    return checkMoistureLimit((int) detection.value);
            default:
                return NO_NOTIFICATION;

        }

    }

    private void initElderLimits(SharedPreferences limitsSharedPreferences){
        //initializing elder limits
        maxHeartBeatLimit = limitsSharedPreferences.getInt
                (SensorsDetectionsConstants.SensorsDetectionLimits.MAX_LIMIT +
                        SensorsDetectionsConstants.SensorTypes.HEARTBEAT_SENSOR, 0);
        minHeartBeatLimit = limitsSharedPreferences.getInt
                (SensorsDetectionsConstants.SensorsDetectionLimits.MIN_LIMIT +
                        SensorsDetectionsConstants.SensorTypes.HEARTBEAT_SENSOR, 0);

        minStepsLimit=limitsSharedPreferences.getInt
                (SensorsDetectionsConstants.SensorsDetectionLimits.MIN_LIMIT +
                        SensorsDetectionsConstants.SensorTypes.STEPS_COUNTER, 0);

        minCaloriesLimit=limitsSharedPreferences.getInt
                (SensorsDetectionsConstants.SensorsDetectionLimits.MIN_LIMIT +
                        SensorsDetectionsConstants.SensorTypes.CALORIES_SENSOR, 0);
    }

    private void initHomeLimits(SharedPreferences limitsSharedPreferences){
        //initializing home limits
        minTemperatureLimit = limitsSharedPreferences.getInt
                (SensorsDetectionsConstants.SensorsDetectionLimits.MIN_LIMIT +
                        SensorsDetectionsConstants.SensorTypes.TEMPERATURE_SENSOR, 0);
        maxTemperatureLimit = limitsSharedPreferences.getInt
                (SensorsDetectionsConstants.SensorsDetectionLimits.MAX_LIMIT +
                        SensorsDetectionsConstants.SensorTypes.TEMPERATURE_SENSOR, 0);

        minMoistureLimit=limitsSharedPreferences.getInt
                (SensorsDetectionsConstants.SensorsDetectionLimits.MIN_LIMIT +
                        SensorsDetectionsConstants.SensorTypes.MOISTURE_SENSOR, 0);
        maxMoistureLimit=limitsSharedPreferences.getInt
                (SensorsDetectionsConstants.SensorsDetectionLimits.MAX_LIMIT +
                        SensorsDetectionsConstants.SensorTypes.MOISTURE_SENSOR, 0);
        maxGasLimit=limitsSharedPreferences.getBoolean(SensorsDetectionsConstants.SensorsDetectionLimits.MAX_LIMIT +
                SensorsDetectionsConstants.SensorTypes.GAS_SENSOR, false);
        maxSmokeLimit=limitsSharedPreferences.getBoolean(SensorsDetectionsConstants.SensorsDetectionLimits.MAX_LIMIT +
                SensorsDetectionsConstants.SensorTypes.SMOKE_SENSOR, false);
    }

    /*Checkers for Elder Values*/

    private String checkHeartbeatLimit(int value){
        if (value > maxHeartBeatLimit){
            return SensorsDetectionsConstants.SensorsDetectionLimits.MAX_LIMIT +
                    SensorsDetectionsConstants.SensorTypes.HEARTBEAT_SENSOR;
        }else if(value<minHeartBeatLimit){
            return SensorsDetectionsConstants.SensorsDetectionLimits.MIN_LIMIT +
                    SensorsDetectionsConstants.SensorTypes.HEARTBEAT_SENSOR;
        }else
            return NO_NOTIFICATION;
    }

    private String checkMaxHeartbeatLimit(int value){
        if (value > maxHeartBeatLimit){
            return SensorsDetectionsConstants.SensorsDetectionLimits.MAX_LIMIT +
                    SensorsDetectionsConstants.SensorTypes.HEARTBEAT_SENSOR;
        }else  return NO_NOTIFICATION;
    }

    private String checkMinHeartbeatLimit(int value){
        if(value<minHeartBeatLimit){
            return SensorsDetectionsConstants.SensorsDetectionLimits.MIN_LIMIT +
                    SensorsDetectionsConstants.SensorTypes.HEARTBEAT_SENSOR;
        }else
            return NO_NOTIFICATION;
    }

    private String checkCaloriesLimit(int value){
        if(value>=minCaloriesLimit)
            return SensorsDetectionsConstants.SensorsDetectionLimits.MIN_LIMIT +
                    SensorsDetectionsConstants.SensorTypes.CALORIES_SENSOR;
        else return NO_NOTIFICATION;
    }

    private String checkStepsLimit(int value){
        if(value>=minStepsLimit){
            return SensorsDetectionsConstants.SensorsDetectionLimits.MIN_LIMIT +
                    SensorsDetectionsConstants.SensorTypes.STEPS_COUNTER;
        }else return NO_NOTIFICATION;
    }

    /*Checkers for Home  Values*/

    private String checkSmokeLimit(int value){
        if (value>SensorsDetectionsConstants.SensorsDetectionLimits.MAX_SMOKE_PERCENTAGE){
            return SensorsDetectionsConstants.SensorsDetectionLimits.MAX_LIMIT +
                    SensorsDetectionsConstants.SensorTypes.SMOKE_SENSOR;
        }else return NO_NOTIFICATION;
    }

    private String checkGasLimit(int value){
        if (value>SensorsDetectionsConstants.SensorsDetectionLimits.MAX_GAS_PERCENTAGE){
            return SensorsDetectionsConstants.SensorsDetectionLimits.MAX_LIMIT +
                    SensorsDetectionsConstants.SensorTypes.GAS_SENSOR;
        }else return NO_NOTIFICATION;
    }

    private String checkTemperatureLimit(int value){
        if(value>maxTemperatureLimit){
            return SensorsDetectionsConstants.SensorsDetectionLimits.MAX_LIMIT +
                    SensorsDetectionsConstants.SensorTypes.TEMPERATURE_SENSOR;
        }else if(value<minTemperatureLimit){
            return SensorsDetectionsConstants.SensorsDetectionLimits.MIN_LIMIT +
                    SensorsDetectionsConstants.SensorTypes.TEMPERATURE_SENSOR;
        }else return NO_NOTIFICATION;
    }

    private String checkMaxTemperatureLimit(int value){
        if(value>maxTemperatureLimit){
            return SensorsDetectionsConstants.SensorsDetectionLimits.MAX_LIMIT +
                    SensorsDetectionsConstants.SensorTypes.TEMPERATURE_SENSOR;
        }else return NO_NOTIFICATION;
    }

    private String checkMinTemperatureLimit(int value){
        if(value<minTemperatureLimit){
            return SensorsDetectionsConstants.SensorsDetectionLimits.MIN_LIMIT +
                    SensorsDetectionsConstants.SensorTypes.TEMPERATURE_SENSOR;
        }else return NO_NOTIFICATION;
    }


    private String checkMoistureLimit(int value){
        if (value>maxMoistureLimit){
            return SensorsDetectionsConstants.SensorsDetectionLimits.MAX_LIMIT +
                    SensorsDetectionsConstants.SensorTypes.MOISTURE_SENSOR;
        }else if(value<minMoistureLimit){
            return SensorsDetectionsConstants.SensorsDetectionLimits.MIN_LIMIT +
                    SensorsDetectionsConstants.SensorTypes.MOISTURE_SENSOR;
        }else return NO_NOTIFICATION;
    }

    private String checkMaxMoistureLimit(int value){
        if (value>maxMoistureLimit){
            return SensorsDetectionsConstants.SensorsDetectionLimits.MAX_LIMIT +
                    SensorsDetectionsConstants.SensorTypes.MOISTURE_SENSOR;
        }else return NO_NOTIFICATION;
    }

    private String checkMinMoistureLimit(int value){
        if(value<minMoistureLimit){
            return SensorsDetectionsConstants.SensorsDetectionLimits.MIN_LIMIT +
                    SensorsDetectionsConstants.SensorTypes.MOISTURE_SENSOR;
        }else return NO_NOTIFICATION;
    }



}
