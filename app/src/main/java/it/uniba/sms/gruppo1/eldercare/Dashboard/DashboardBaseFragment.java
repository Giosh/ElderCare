package it.uniba.sms.gruppo1.eldercare.Dashboard;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import it.uniba.sms.gruppo1.eldercare.Constants.*;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import it.uniba.sms.gruppo1.eldercare.RegistrationActivities.LoginActivity;
import it.uniba.sms.gruppo1.eldercare.Models.SensorDetectionModel;

import static it.uniba.sms.gruppo1.eldercare.Constants.CollectionNames.SENSORS_DETECTIONS;

/*This Fragment contains all the features and characteristics that the two
* fragments (elderFragment, and HouseFragment) in the Dashboard share. */

public abstract class DashboardBaseFragment extends Fragment {

    private static final String TAG = "DashBoardBaseFragment";
    private FirebaseFirestore mDatabase;
    private CollectionReference mElderSensorsDetections;
    private OnFragmentInteractionListener mListener;
    private String landScapeTest=" landscape ";
    private Context thisContext;




    public DashboardBaseFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mDatabase = FirebaseFirestore.getInstance();
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mElderSensorsDetections = mDatabase.collection(CollectionNames.ELDERS_COLLECTION_NAME).
                document(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getEmail()).
                collection(SENSORS_DETECTIONS);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener && context instanceof DashboardElderFragment.ElderDashboardElementsUpdater &&
                context instanceof DashboardHomeFragment.HomeDashboardElementsUpdater) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener, ElderDashboardElementsUpdater end HomeDashboardElementsUpdater)");
        }
    }

    /*Add the listener that will notify the main activity whenever new detections arrive
    *through the method updateUiWithData, so that the new sensorsDetections can be displayed
    * on the DashBoard. This is an abstract method, and each Fragment gives its own implementation.
     */

    @Override
    public void onStart() {
        super.onStart();
        thisContext=getContext();
        mElderSensorsDetections.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(QuerySnapshot queryDocumentSnapshots,FirebaseFirestoreException e) {
                List<DocumentChange> lastAddedSensorsDetections = queryDocumentSnapshots.getDocumentChanges();
                ArrayList<SensorDetectionModel> dataToDisplay   = new ArrayList<>();

                for (DocumentChange d: lastAddedSensorsDetections){
                    try {
                        dataToDisplay.add(d.getDocument().toObject(SensorDetectionModel.class));
                    } catch (RuntimeException exc){
                        Log.e(TAG,"AAAA "+exc.getMessage());
                    }
                }
                updateUiWithData(dataToDisplay);
            }
        });
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    public abstract void updateUiWithData(ArrayList<SensorDetectionModel> lastSensorsDetections);



}
