# ElderCare
### Android Mobile Application

This application is targeted to people who are interested in taking care of a needy individual, more specifically an elderly. 


Use cases:

1. Track the state of health of your registered needy individual through real-time sensors detections.
These sensors monitor both physical parameters (e.g heartbeat, preassure) 
and environmental parameters (e.g. home temperature, smoke detection).

2. Set monitored parameters thresholds in order for the app to worn you about unusual conditions.

3. Be always updated about his/her physical and environmental conditions.

4. Keep records of his/her physical and environmental conditions and view how they change over time.

Ultimately, your elderly won't be afraid of beeing lefted alone.

Detailed explanation of how the application works can be found at this link:
[Link](https://drive.google.com/file/d/1qV2Ot66byv5ECxkeMDVpohp5hp0WnNKW/view?usp=sharing)

Overview of the application architecture:

![application_elder_care_overview](/uploads/d41adf9b34231e5d9168510e6ba65fbf/application_elder_care_overview.png)

