package it.uniba.sms.gruppo1.triggerapp.Models;

import android.util.Log;

import java.util.Random;

import static it.uniba.sms.gruppo1.triggerapp.MainActivity.TAG;

public class  SensorModel {
    // This enum defines sensors types that we can have in the app.
    public enum S_type {moisture, smoke, gas, temperature, heart, calories, steps, sleep}

    // Sensor properties: min/max value of measure and its type
    private int min;
    private int max;
    private int somma = 0;

    private S_type type;

    // Just specify the sensor type (using the S_type enum) to create the object
    public SensorModel(S_type type) {
        this.type = type;
        reset();
    }

    // This method reset the min/max value of the Sensor when it's called.
    // It's useful in the constructor to automatically set the range or to reset after the
    // anomaly setting.
    public void reset() {
        switch (this.type) {
            case moisture:
                this.min = 0;
                this.max = 100;
                break;
            case smoke:
                this.min = 0;
                this.max = 20;
                break;
            case gas:
                this.min = 0;
                this.max = 15;
                break;
            case temperature:
                this.min = 20;
                this.max = 32;
                break;
            case heart:
                this.min = 80;
                this.max = 120;
                break;
            case calories:
                this.min = 0;
                this.max = 10;
                break;
            case steps:
                this.min = 0;
                this.max = 3;
                break;
            case sleep:
                this.min = 1;
                this.max = 3;
                break;
        }
    }


    // This method is useful to generate random value between a mimum and a maximum
    public float getValue() {
        int bound = (this.max - this.min);

        // If it's steps / calories sensor, then it sends the sum of the value
        if (type == S_type.calories || type == S_type.steps) {
            this.somma += (float) new Random().nextInt(bound) + this.min;
            return somma;
        } else
            return (float) new Random().nextInt(bound) + this.min;
    }
}
