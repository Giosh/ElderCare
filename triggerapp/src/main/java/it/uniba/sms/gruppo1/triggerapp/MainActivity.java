package it.uniba.sms.gruppo1.triggerapp;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.WriteBatch;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import it.uniba.sms.gruppo1.triggerapp.Models.SensorModel;


public class MainActivity extends AppCompatActivity {
    public static final String TAG = "MainActivity";
    private FirebaseFirestore db;

    private Timer timer;
    private boolean sendData = false;

    private ToggleButton toggleSend;
    private Button loadDayBefore;
    private EditText editTextEmail;
    private EditText measure;
    private HashMap<SensorModel.S_type, SensorModel> sensors = new HashMap<SensorModel.S_type, SensorModel>();
    private Button sendAnomaly;
    private String sensorSelected = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Setup the dropdown menu to select the sensor associated to the manual relevation sending
        Spinner spinner = findViewById(R.id.spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                MainActivity.this,
                R.layout.support_simple_spinner_dropdown_item,
                getResources().getStringArray(R.array.sensors));
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);
        spinner.setSelection(0);


        // Inizialize DB
        db = FirebaseFirestore.getInstance();

        // Inizialize view elements
        toggleSend     = findViewById(R.id.startSend);
        loadDayBefore  = findViewById(R.id.dayBefore);
        editTextEmail  = findViewById(R.id.editTextEmail);
        sendAnomaly    = findViewById(R.id.sendAnom);
        measure        = findViewById(R.id.measure);

        // Get the sensor string when one is selected
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                sensorSelected = getResources().getStringArray(R.array.sensors)[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                sensorSelected = getResources().getStringArray(R.array.sensors)[0];
            }
        });

        // Assigning for each switchSensor element (there all the switches inside!) the listener
        sendAnomaly.setOnClickListener(new View.OnClickListener() {
                     public void onClick(View v) {
                                 if (!editTextEmail.getText().toString().equals("") && !measure.getText().toString().equals("")) {
                                     sendAnomaly(
                                             sensorSelected,
                                             Float.parseFloat(measure.getText().toString()),
                                             editTextEmail.getText().toString()
                                     );
                                 }
                                 else {
                                     Toast.makeText(getApplicationContext(), getString(R.string.alert), Toast.LENGTH_SHORT).show();
                                 }

                     }
        });

        loadDayBefore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(editTextEmail.getText().toString().isEmpty()) {
                    Toast.makeText(getApplicationContext(), getString(R.string.alert), Toast.LENGTH_SHORT).show();
                }else {
                    generateLastDay(editTextEmail.getText().toString());
                }
            }
        });

        // Inizialize sensor objects.
        //
        // For each sensor type, create an object and insert it in a map having its type as key
        // In order to access to the gas sensor, just run sensors.get(SensorModel.S_type.gas).
        // Pay attenction to the syntax, the type is an enum located in SensorModel.java !!!
        if (sensors.size() == 0) {
            for (SensorModel.S_type type : SensorModel.S_type.values()) {
                sensors.put(type, new SensorModel(type));
            }
        }

        //Declare the timer
        timer = new Timer();
        sendData = false;
        //Set the schedule function and rate
        timer.scheduleAtFixedRate(new TimerTask() {
              @Override
              public void run() {
                  //Called each time when 10000 milliseconds (10 seconds) (the period parameter)
                  if(sendData == true) {
                      addSensorData(editTextEmail.getText().toString(), sensors);
                  }
              }
        },
        //Set how long before to start calling the TimerTask (in milliseconds)
        0,
        //Set the amount of time between each execution (in milliseconds)
        10000);

        // This listener manages the 'Send' toggle button. When checked it enables
        // the continuous sending of data to the database.
        toggleSend.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if ( buttonView.getId() == R.id.startSend) {

                    // If there's no email setted up stop!
                    if(editTextEmail.getText().toString().isEmpty()) {
                        sendData = false;
                        Toast.makeText(getApplicationContext(), getString(R.string.alert), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    sendData = true;
                }
                else {
                    sendData = false;
                }
            }
        });
    }


    public void sendAnomaly(String sensorType, float n, String elder) {
        Map<String, Object> user = new HashMap<>();
        user.put("sensorType", sensorType);
        user.put("timestamp", FieldValue.serverTimestamp());
        user.put("value", n);

        db.collection("elders").document(elder)
                .collection("sensorsDetections")
                .add(user);
    }

    // This method generate records of measure for each sensor and send them to the database.
    // As parameters it needs <elder> as key on the db and the map sensors, that's inizialized
    // in the onCreate() method with all the sensors. Due to the fact that the map contains
    // all the sensors, for adding data to the database we iterate through it.
    public void addSensorData(String elder, HashMap<SensorModel.S_type, SensorModel> sensors) {
        for (Map.Entry<SensorModel.S_type, SensorModel> entry : sensors.entrySet()) {

            Map<String, Object> user = new HashMap<>();
            user.put("sensorType", entry.getKey().toString());
            user.put("timestamp", FieldValue.serverTimestamp());

            // During real-time sending, the elder isn't sleeping
            if (entry.getKey().equals(SensorModel.S_type.sleep)) {
                user.put("value", 0);
            }
            else
                user.put("value", entry.getValue().getValue());

            // Add a new document with a generated ID
            db.collection("elders").document(elder)
                    .collection("sensorsDetections")
                    .add(user);
            user.clear();
        }
    }

    public void generateLastDay(String elder) {
            // Get last day
            Calendar date = new GregorianCalendar();

             // reset hour, minutes, seconds and millis
            date.set(Calendar.HOUR_OF_DAY, 0);
            date.set(Calendar.MINUTE, 0);
            date.set(Calendar.SECOND, 0);
            date.set(Calendar.MILLISECOND, 0);
            date.add(Calendar.DATE, -1);

            WriteBatch batch = db.batch();

            // In one day there are 24 hours, so insert one measure for each hour, for each sensor
            // from the 00:00 of the first day to the 09:00 of the second day-> 32 hours
            for (int i = 0; i < 32; i++) {

                for (Map.Entry<SensorModel.S_type, SensorModel> entry : sensors.entrySet()) {
                    Map<String, Object> user = new HashMap<>();

                    // Generate sleep measure only if it's in the between of 20.00 and 8.00
                    if (date.get(Calendar.HOUR_OF_DAY) > 7
                            && date.get(Calendar.HOUR_OF_DAY) < 20
                            && entry.getKey().toString().equals(SensorModel.S_type.sleep.toString()))
                        continue;

                    user.put("sensorType",  entry.getKey().toString());
                    user.put("value",       entry.getValue().getValue());
                    user.put("timestamp",   new Timestamp(date.getTime()));

                    DocumentReference d = db.collection("elders").document(elder).collection("sensorsDetections").document();
                    batch.set(d, user);
                }
                date.add(Calendar.HOUR, 1);
            }
            batch.commit().addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    Toast.makeText(getApplicationContext(), "Caricamento completato", Toast.LENGTH_SHORT).show();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.d(TAG, "Commit fallito! " +e.getMessage());
                }
            });
    }
}

